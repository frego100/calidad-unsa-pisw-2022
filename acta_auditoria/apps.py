from django.apps import AppConfig


class ActaAuditoriaConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'acta_auditoria'
