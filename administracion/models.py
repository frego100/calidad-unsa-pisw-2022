from django.db import models

# Create your models here.
class Proceso(models.Model):
    nombre = models.CharField(max_length=150)

class Dependencia(models.Model):
    nombre = models.CharField(max_length=150)
    facultad = models.BooleanField(default=False)
    escuela = models.BooleanField(default=False)

class Clausula(models.Model):
    nombre = models.CharField(max_length=10)

class Facultad(models.Model):
    nombre = models.CharField(max_length=150)

class Escuela(models.Model):
    facultad = models.ForeignKey(Facultad,null=True,blank=True,on_delete=models.CASCADE)
    nombre = models.CharField(max_length=150)

class CorreoAdministrador(models.Model):
    correo = models.CharField(max_length=150)