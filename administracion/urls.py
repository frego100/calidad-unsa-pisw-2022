from django.urls import path, re_path
from . import views

urlpatterns = [
        path("listarProcesos",views.listarProcesos , name='procesos'),
        path("listarDependencias",views.listarDependencias , name='dependencias'),
        re_path(r'editproceso/(?P<data_id>[0-9]+)$',
            views.EditProceso.as_view(), name='edit-proceso'),
        path("proceso", views.ProcesoVista.as_view(), name='proceso'),
        path('delete_proceso/<int:data_id>', views.DeleteProceso, name='delete-proceso'),

        re_path(r'editdependencia/(?P<data_id>[0-9]+)$',
            views.EditDependencia.as_view(), name='edit-dependencia'),
        path("dependencia", views.DependenciaVista.as_view(), name='dependencia'),
        path('delete_dependencia/<int:data_id>', views.DeleteDependencia, name='delete-dependencia'),

        re_path(r'editclausula/(?P<data_id>[0-9]+)$',
            views.EditClausula.as_view(), name='edit-clausula'),
        path("clausula", views.ClausulaVista.as_view(), name='clausula'),
        path('delete_clausula/<int:data_id>', views.DeleteClausula, name='delete-clausula'),

        re_path(r'editfacultad/(?P<data_id>[0-9]+)$',
            views.EditFacultad.as_view(), name='edit-facultad'),
        path("facultad", views.FacultadVista.as_view(), name='facultad'),
        path('delete_facultad/<int:data_id>', views.DeleteFacultad, name='delete-facultad'),

        re_path(r'editescuela/(?P<data_id>[0-9]+)$',
            views.EditEscuela.as_view(), name='edit-escuela'),
        path("escuela", views.EscuelaVista.as_view(), name='escuela'),
        path('delete_escuela/<int:data_id>', views.DeleteEscuela, name='delete-escuela'),
]
