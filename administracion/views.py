from django.shortcuts import render, redirect
from django.views import View
from administracion.models import Proceso, Facultad, Escuela, Clausula
from administracion.models import Dependencia
from usuarios_auditoria.models import Usuario
from django.http import JsonResponse
# Create your views here.


def listarProcesos(_request):
    procesos = list(Proceso.objects.values())

    if (len(procesos) > 0):
        data = {'message': "Success", 'objetos': procesos}
    else:
        data = {'message': "Not Found"}

    return JsonResponse(data)


def listarDependencias(_request):
    #form = ProgramaForm()
    depencias = list(Dependencia.objects.values())

    if (len(depencias) > 0):
        data = {'message': "Success", 'objetos': depencias}
    else:
        data = {'message': "Not Found"}

    return JsonResponse(data)

############# PROCESOS ##############################


class ProcesoVista(View):
    def get(self, request):
        if not request.user.is_authenticated:
            return  redirect("login")   
        if request.user.is_authenticated and Usuario.objects.get(user=request.user).rol != "administrador":
            return  redirect("principal")
        auth_usuario = request.user
        context = {}
        if request.user.is_authenticated:
            usuario = Usuario.objects.get(user=auth_usuario)
            context["user"] = usuario
            procesos = list(Proceso.objects.values())  # Proceso.objects.all()
            context["procesos"] = procesos
            return render(request, "proceso-view.html", context)
        return redirect("principal")

    def post(self, request,):
        if not request.user.is_authenticated:
            return  redirect("login")   
        if request.user.is_authenticated and Usuario.objects.get(user=request.user).rol != "administrador":
            return  redirect("principal")
        proceso = Proceso.objects.create(
            nombre=request.POST.get("nombre")
        )

        return redirect("proceso")


class EditProceso(View):
    def get(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return  redirect("login")   
        if request.user.is_authenticated and Usuario.objects.get(user=request.user).rol != "administrador":
            return  redirect("principal")
        proceso = Proceso.objects.get(id=self.kwargs['data_id'])

        context = {
            'proceso': proceso
        }
        return redirect("principal")

    def post(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return  redirect("login")   
        if request.user.is_authenticated and Usuario.objects.get(user=request.user).rol != "administrador":
            return  redirect("principal")
        proceso = Proceso.objects.get(id=self.kwargs['data_id'])
        proceso.nombre = request.POST.get("nombre")
        proceso.save()
        return redirect("proceso")


def DeleteProceso(_request, data_id):

    if not _request.user.is_authenticated:
        return  redirect("login")   
    if _request.user.is_authenticated and Usuario.objects.get(user=_request.user).rol != "administrador":
        return  redirect("principal")

    try:
        proceso = Proceso.objects.get(id=data_id)
        proceso.delete()
        data = {'message': "Success"}
    except:
        data = {'message': "Not Found"}
    return redirect("proceso")
############################################


############# DEPENDENCIAS ##############################
class DependenciaVista(View):
    def get(self, request):
        if not request.user.is_authenticated:
            return  redirect("login")   
        if request.user.is_authenticated and Usuario.objects.get(user=request.user).rol != "administrador":
            return  redirect("principal")

        auth_usuario = request.user
        context = {}
        if request.user.is_authenticated:
            usuario = Usuario.objects.get(user=auth_usuario)
            context["user"] = usuario
            dependencias = list(Dependencia.objects.values())  # Proceso.objects.all()
            context["dependencias"] = dependencias
            return render(request, "dependencia-view.html", context)
        return redirect("principal")

    def post(self, request,):
        if not request.user.is_authenticated:
            return  redirect("login")   
        if request.user.is_authenticated and Usuario.objects.get(user=request.user).rol != "administrador":
            return  redirect("principal")

        seleccion = request.POST.get("seleccion_add")
        dependencia = Dependencia.objects.create(
            nombre=request.POST.get("nombre"),
            facultad= True if (seleccion=="1" or seleccion=="2") else False,
            escuela= True if (seleccion=="2") else False
        )

        return redirect("dependencia")


class EditDependencia(View):
    def get(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return  redirect("login")   
        if request.user.is_authenticated and Usuario.objects.get(user=request.user).rol != "administrador":
            return  redirect("principal")

        dependencia = Dependencia.objects.get(id=self.kwargs['data_id'])

        context = {
            'dependencia': dependencia
        }
        return redirect("principal")

    def post(self, request, *args, **kwargs):

        if not request.user.is_authenticated:
            return  redirect("login")   
        if request.user.is_authenticated and Usuario.objects.get(user=request.user).rol != "administrador":
            return  redirect("principal")


        dependencia = Dependencia.objects.get(id=self.kwargs['data_id'])
        dependencia.nombre = request.POST.get("edit_nombre")
        seleccion = request.POST.get("seleccion_edit")
        dependencia.facultad = True if (seleccion == "1" or seleccion == "2") else False
        dependencia.escuela = True if (seleccion == "2") else False
        dependencia.save()
        return redirect("dependencia")


def DeleteDependencia(_request, data_id):
    if not _request.user.is_authenticated:
        return  redirect("login")   
    if _request.user.is_authenticated and Usuario.objects.get(user=_request.user).rol != "administrador":
        return  redirect("principal")

    try:
        dependencia = Dependencia.objects.get(id=data_id)
        dependencia.delete()
        data = {'message': "Success"}
    except:
        data = {'message': "Not Found"}
    return redirect("dependencia")
############################################

############# Clausula ##############################


class ClausulaVista(View):
    def get(self, request):

        if not request.user.is_authenticated:
            return  redirect("login")   
        if request.user.is_authenticated and Usuario.objects.get(user=request.user).rol != "administrador":
            return  redirect("principal")

        auth_usuario = request.user
        context = {}
        if request.user.is_authenticated:
            usuario = Usuario.objects.get(user=auth_usuario)
            context["user"] = usuario
            clausulas = list(Clausula.objects.values())  # Proceso.objects.all()
            context["clausulas"] = clausulas
            return render(request, "clausula-view.html", context)
        return redirect("principal")

    def post(self, request,):
        if not request.user.is_authenticated:
            return  redirect("login")   
        if request.user.is_authenticated and Usuario.objects.get(user=request.user).rol != "administrador":
            return  redirect("principal")

        clausula = Clausula.objects.create(
            nombre=request.POST.get("nombre")
        )

        return redirect("clausula")


class EditClausula(View):
    def get(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return  redirect("login")   
        if request.user.is_authenticated and Usuario.objects.get(user=request.user).rol != "administrador":
            return  redirect("principal")
        clausula = Clausula.objects.get(id=self.kwargs['data_id'])

        context = {
            'clausula': clausula
        }
        return redirect("principal")

    def post(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return  redirect("login")   
        if request.user.is_authenticated and Usuario.objects.get(user=request.user).rol != "administrador":
            return  redirect("principal")
        clausula = Clausula.objects.get(id=self.kwargs['data_id'])
        clausula.nombre = request.POST.get("nombre")
        clausula.save()
        return redirect("clausula")


def DeleteClausula(_request, data_id):
    if not _request.user.is_authenticated:
        return  redirect("login")   
    if _request.user.is_authenticated and Usuario.objects.get(user=_request.user).rol != "administrador":
        return  redirect("principal")


    try:
        clausula = Clausula.objects.get(id=data_id)
        clausula.delete()
        data = {'message': "Success"}
    except:
        data = {'message': "Not Found"}
    return redirect("clausula")
############################################

############# Facultad ##############################


class FacultadVista(View):
    def get(self, request):
        if not request.user.is_authenticated:
            return  redirect("login")   
        if request.user.is_authenticated and Usuario.objects.get(user=request.user).rol != "administrador":
            return  redirect("principal")
        auth_usuario = request.user
        context = {}
        if request.user.is_authenticated:
            usuario = Usuario.objects.get(user=auth_usuario)
            context["user"] = usuario
            facultades = list(Facultad.objects.values())  # Proceso.objects.all()
            context["facultades"] = facultades
            return render(request, "facultad-view.html", context)
        return redirect("principal")

    def post(self, request,):
        if not request.user.is_authenticated:
            return  redirect("login")   
        if request.user.is_authenticated and Usuario.objects.get(user=request.user).rol != "administrador":
            return  redirect("principal")
            
        facultad = Facultad.objects.create(
            nombre=request.POST.get("nombre")
        )

        return redirect("facultad")


class EditFacultad(View):
    def get(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return  redirect("login")   
        if request.user.is_authenticated and Usuario.objects.get(user=request.user).rol != "administrador":
            return  redirect("principal")

        facultad = Facultad.objects.get(id=self.kwargs['data_id'])

        context = {
            'facultad': facultad
        }
        return redirect("principal")

    def post(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return  redirect("login")   
        if request.user.is_authenticated and Usuario.objects.get(user=request.user).rol != "administrador":
            return  redirect("principal")
        facultad = Facultad.objects.get(id=self.kwargs['data_id'])
        facultad.nombre = request.POST.get("nombre")
        facultad.save()
        return redirect("facultad")


def DeleteFacultad(_request, data_id):
    if not _request.user.is_authenticated:
        return  redirect("login")   
    if _request.user.is_authenticated and Usuario.objects.get(user=_request.user).rol != "administrador":
        return  redirect("principal")


    try:
        facultad = Facultad.objects.get(id=data_id)
        facultad.delete()
        data = {'message': "Success"}
    except:
        data = {'message': "Not Found"}
    return redirect("facultad")
############################################

############# Escuela ##############################


class EscuelaVista(View):
    def get(self, request):
        if not request.user.is_authenticated:
            return  redirect("login")   
        if request.user.is_authenticated and Usuario.objects.get(user=request.user).rol != "administrador":
            return  redirect("principal")
        auth_usuario = request.user
        context = {}
        if request.user.is_authenticated:
            usuario = Usuario.objects.get(user=auth_usuario)
            context["user"] = usuario
            escuelas = list(Escuela.objects.values())  # Proceso.objects.all()
            context["escuelas"] = escuelas
            facultades = list(Facultad.objects.values())  # Proceso.objects.all()
            context["facultades"] = facultades
            return render(request, "escuela-view.html", context)
        return redirect("principal")


    def post(self, request,):
        if not request.user.is_authenticated:
            return  redirect("login")   
        if request.user.is_authenticated and Usuario.objects.get(user=request.user).rol != "administrador":
            return  redirect("principal")


        if request.POST.get("nombre") == "":
            return redirect("escuela")

        elif request.POST.get("nombre") != "" and not Escuela.objects.filter(nombre=request.POST.get("nombre")).exists() :
            escuela = Escuela.objects.create(
                facultad_id=request.POST.get("facultad_id_add"),
                nombre=request.POST.get("nombre")
            )

        return redirect("escuela")


class EditEscuela(View):
    def get(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return  redirect("login")   
        if request.user.is_authenticated and Usuario.objects.get(user=request.user).rol != "administrador":
            return  redirect("principal")

        facultades = list(Facultad.objects.values())  # Proceso.objects.all()
        context["facultades"] = facultades
        escuela = Escuela.objects.get(id=self.kwargs['data_id'])
        context = {
            'escuela': escuela
        }
        return redirect("escuela")

    def post(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return  redirect("login")   
        if request.user.is_authenticated and Usuario.objects.get(user=request.user).rol != "administrador":
            return  redirect("principal")


        escuela = Escuela.objects.get(id=self.kwargs['data_id'])
        
        if request.POST.get("nombre") == "":
            return redirect("escuela")
        elif Escuela.objects.filter(nombre=request.POST.get("nombre")).exists():
            escuela.facultad_id = request.POST.get("facultad_id_edit")
            escuela.save()
        else :
            escuela.facultad_id = request.POST.get("facultad_id_edit")
            escuela.nombre = request.POST.get("nombre")
            escuela.save()
            
        return redirect("escuela")


def DeleteEscuela(_request, data_id):
    if not _request.user.is_authenticated:
        return  redirect("login")   
    if _request.user.is_authenticated and Usuario.objects.get(user=_request.user).rol != "administrador":
        return  redirect("principal")

    try:
        escuela = Escuela.objects.get(id=data_id)
        escuela.delete()
        data = {'message': "Success"}
    except:
        data = {'message': "Not Found"}
    return redirect("escuela")
############################################
