from django.apps import AppConfig


class GeneracionPdfsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'generacion_pdfs'
