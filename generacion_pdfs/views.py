#Librerias de generacion de docx y pdf
from django.shortcuts import render
from django.shortcuts import render, redirect
from docx import Document
from io import BytesIO
from os import remove
from django.http import HttpResponse, FileResponse
from docx2pdf import convert
from docx.enum.text import WD_ALIGN_PARAGRAPH 
from docx.shared import Pt
from docx.shared import Inches
from docx.enum.style import WD_STYLE_TYPE
from docx.shared import Cm, Inches
from docx.enum.section import WD_ORIENT
from docx.shared import Mm
from docx.enum.table import WD_TABLE_ALIGNMENT
#Modelos de programa y plan
from datetime import datetime
from plan_auditoria.models import PlanAuditoria, BloqueProcesoPlan, ProcesoPlanAuditoria, PlanAuditores, PlanFunciones, ClausulaProcesoPlan, EquipoProcesoPlan
from programa_auditoria.models import ProgramaAuditoria, ProcesoPrograma,ActividadesProcesoPrograma,DependenciasProcesoPrograma
from administracion.models import Proceso, Dependencia, Facultad, Escuela, Clausula
from docx.oxml.shared import OxmlElement
from docx.oxml.ns import qn

def set_cell_margins(cell, **kwargs):
    tc = cell._tc
    tcPr = tc.get_or_add_tcPr()
    tcMar = OxmlElement('w:tcMar')

    for m in ["top", "start", "bottom", "end"]:
        if m in kwargs:
            node = OxmlElement("w:{}".format(m))
            node.set(qn('w:w'), str(kwargs.get(m)))
            node.set(qn('w:type'), 'dxa')
            tcMar.append(node)

    tcPr.append(tcMar)

def generarPlanesPdfs(id_programa):
    print("G id programa", id_programa)
    planes = list(PlanAuditoria.objects.filter(programa_auditoria_id = id_programa))
    for plan in planes:
        print("G id plan", plan.id)
        crearPdfPlan(plan.id)

def listaOraciones(cadena):
    seperador = "\n"
    arregloDeSubCadenas = cadena.split(seperador)
    return arregloDeSubCadenas

def inicialesNombre(nombre):
    seperador = " "
    arregloDeSubCadenas = nombre.split(seperador)
    cadenaNueva = ""

    for i in range(len(arregloDeSubCadenas)):
        cadenaNueva += arregloDeSubCadenas[i][0:1]

    return cadenaNueva
def crearPdfPrograma(id_programa):
    programa = ProgramaAuditoria.objects.get(id=id_programa)
    
    ruta_firma = "files/" + str(programa.firma)
    '''try:
        ruta_firma = "files/" + str(programa.firma)
        print("Ruta: ", ruta_firma)
    except Exception as error:
        print(error)'''
    
    ArrayPadre = []
    FechasInicio = []
    FechasFin = []
    procesos_programa = ProcesoPrograma.objects.filter(programa_auditoria=programa)

    i = 0
    for procesos_unicos in procesos_programa:
        actividades = list(ActividadesProcesoPrograma.objects.filter(proceso_programa=procesos_unicos))
        actividades_nombres_temporal = []
        for actividad in actividades:
            actividades_nombres_temporal += [Proceso.objects.get(id = actividad.proceso.id).nombre]

        dependencias_programa = DependenciasProcesoPrograma.objects.filter(proceso_programa=procesos_unicos)
        dependencias_nombres_temporal = []
        for dependencia_programa in dependencias_programa:
            dependencias_nombres_temporal += [Dependencia.objects.get(id = dependencia_programa.dependencia.id).nombre]

        ArrayPadre += [actividades_nombres_temporal, [procesos_unicos.fecha_inicio.strftime('%d.%m.%Y'), procesos_unicos.fecha_fin.strftime('%d.%m.%Y')], dependencias_nombres_temporal]
        FechasInicio += [procesos_unicos.fecha_inicio]
        FechasFin += [procesos_unicos.fecha_fin]
        i+=1

    #print("Programa", programa)
    #print("Detalles programa", ArrayPadre)

    menorFecha = min(FechasInicio)
    mayorFecha = max(FechasFin)

    #Lista de grupo
    lista_grupo = ""
    planes = PlanAuditoria.objects.filter(programa_auditoria = programa )
    
    bloques =[]
    for plan in planes :
        bloques += list(BloqueProcesoPlan.objects.filter( plan_auditoria = plan))

    for bloque in bloques:
        if bloque.facultad:
            lista_grupo += bloque.facultad.nombre + ", "
        elif bloque.escuela:
            lista_grupo += bloque.escuela.nombre + ", "
    
    #print("Bloques ",bloques)
    #print("Lista grupo ",lista_grupo)

    # Creación del documento word
    document = Document()
    style = document.styles['Normal']
    font = style.font
    font.name = 'Calibri'
    font.size = Pt(10)

    font_styles = document.styles
    font_charstyle = font_styles.add_style('CommentsStyle', WD_STYLE_TYPE.CHARACTER)
    font_object = font_charstyle.font
    font_object.size = Pt(11)
    font_object.name = 'Calibri'

    section = document.sections[0]
    section.orientation = WD_ORIENT.LANDSCAPE
    section.page_width = Mm(215)  # for A4 paper
    section.page_height = Mm(280)

    #Margen de página
    section.top_margin = Cm(0.5)
    section.bottom_margin = Cm(0.5)
    section.left_margin = Cm(1.5)
    section.right_margin = Cm(1.5)
    header = section.header
    section.header_distance=Cm(0.5)

    # Agragado de elemento al documento word
    #header
    section = document.sections[0]
    header = section.header
    table=header.add_table(rows=1,cols=4,width=13000)
    table.style = 'TableGrid'
    
    #De este modo se puede agregar una imagen a una celda
    cells=table.add_row().cells
    paragraph=cells[0].paragraphs[0]
    run=paragraph.add_run()
    run.add_picture("static/images/unsa.png", width=Inches(1.6), height=Inches(0.6))

    table.rows[0].cells[1].text="Formato"
    table.rows[0].cells[1].width=Inches(3.13)
    table.rows[0].cells[1].paragraphs[0].alignment = 1
    table.rows[0].cells[2].text="Código"
    table.rows[0].cells[2].width=Inches(0.8)
    table.rows[0].cells[3].text= programa.codigo #Variable de Codigo
    table.rows[0].cells[3].width=Inches(1.5)

    #Variable de código
    titulo="PROGRAMA DE AUDITORÍA DEL SGC"
    cells[1].text=titulo
    table.rows[1].cells[1].paragraphs[0].alignment = 1
    cells[2].text="Versión"
    cells[3].text=programa.version #Variable de Versión
    
    cells=table.add_row().cells

    #Margenes para la imagen luego de crear las filas
    set_cell_margins(table.rows[0].cells[0], top=100, start=100, end=100)
    set_cell_margins(table.rows[1].cells[0], top=100, bottom=100, end=100)

    cells[2].text="Revisión"
    cells[3].text=str(programa.n_revision) #De momento no sé como calcular solo cuantas pag tendrá
    table.rows[1].cells[1]
    #De este modo se puede unir celdas
    table.cell(0, 0).merge(table.cell(2,0))
    table.cell(1, 1).merge(table.cell(2,1))
    #Para separar tablas
    document.add_paragraph('')
    
    #variables
    '''ProcesoAuditoria = ["Sustentación y Aprobación", "Administración Documentaria"]
    Fechas=['29.03.22','31.03.22']
    Auditados= ["Decanos de Facultades"]
    ProcesoAuditoria2 = ["Sustentación", "Administración Documentaria","aea"]
    Fechas2=['29.03.22','31.03.22']
    Auditados2= ["Decanos de Facultades"]
    ArrayPadre=[ProcesoAuditoria,Fechas,Auditados,ProcesoAuditoria,Fechas,Auditados,ProcesoAuditoria2,Fechas2,Auditados2,ProcesoAuditoria,Fechas,Auditados]'''
    LoopCant=len(ArrayPadre)//3

    a=0
    NumeroDeProcesos=0
    for x in range(LoopCant):
        NumeroDeProcesos=len(ArrayPadre[a])+NumeroDeProcesos
        a=a+3
   
    # es mas 4 por la primera fila, la fila cabecesar de la tabla y las 2 ultimas
    filas=NumeroDeProcesos+4

    table=document.add_table(rows=filas,cols=3,style="Table Grid")
    ObjB='OBJETIVO: '
    ObjT=programa.objetivo
    table.rows[0].cells[0].paragraphs[0].add_run(ObjB,style = 'CommentsStyle').bold = True
    table.rows[0].cells[0].paragraphs[0].add_run(ObjT+"\n"+"\n",style = 'CommentsStyle')
    AlcB='ALCANCE: '
    AlcT=programa.alcance
    table.rows[0].cells[0].paragraphs[0].add_run(AlcB,style = 'CommentsStyle').bold = True
    table.rows[0].cells[0].paragraphs[0].add_run(AlcT+"\n"+"\n",style = 'CommentsStyle')
    CriB='CRITERIO DE AUDITORÍA: '
    CriT=programa.criterios_auditoria
    table.rows[0].cells[0].paragraphs[0].add_run(CriB,style = 'CommentsStyle').bold = True
    table.rows[0].cells[0].paragraphs[0].add_run("\n"+CriT+"\n"+"\n",style = 'CommentsStyle')
    MOB='MODALIDAD: '
    MOT=programa.modalidad
    table.rows[0].cells[0].paragraphs[0].add_run(MOB,style = 'CommentsStyle').bold = True
    table.rows[0].cells[0].paragraphs[0].add_run(MOT+"\n"+"\n",style = 'CommentsStyle')
    LUB='LUGAR Y PERIODO DE AUDITORÍA: '
    #LUT='Plataforma meet, Del 29 de marzo al 13 de mayo de 2022 '
    LUT = programa.lugar + ", Del " + menorFecha.strftime('%d.%m.%Y') + " al " + mayorFecha.strftime('%d.%m.%Y')
    table.rows[0].cells[0].paragraphs[0].add_run(LUB,style = 'CommentsStyle').bold = True
    table.rows[0].cells[0].paragraphs[0].add_run("\n"+LUT+"\n"+"\n",style = 'CommentsStyle')
    RecB='RECURSOS REQUERIDOS: '
    RecT=programa.recursos
    table.rows[0].cells[0].paragraphs[0].add_run(RecB,style = 'CommentsStyle').bold = True
    table.rows[0].cells[0].paragraphs[0].add_run("\n"+RecT+"\n"+"\n",style = 'CommentsStyle')
    CRB='CONTROL DE RIESGO DE AUDITORIA:'
    CRT=programa.control_riesgo
    table.rows[0].cells[0].paragraphs[0].add_run(CRB,style = 'CommentsStyle').bold = True
    
    table.rows[0].cells[0].paragraphs[0].add_run("\n"+CRT+"\n",style = 'CommentsStyle')
    

    #con esto se juntan en una
    table.cell(0, 0).merge(table.cell(0,2))

    table.rows[1].cells[0].paragraphs[0].add_run('Proceso a auditar').bold = True
    table.rows[1].cells[0].paragraphs[0].alignment = 1
    
    table.rows[1].cells[1].paragraphs[0].add_run('Fecha (s)').bold = True
    table.rows[1].cells[1].paragraphs[0].alignment = 1
    
    table.rows[1].cells[2].paragraphs[0].add_run('Auditados').bold = True
    table.rows[1].cells[2].paragraphs[0].alignment = 1


    cont=2
    Cont2=0
    C3=0
    MeIn=2

    
    for y in range(LoopCant):
        CantPro=len(ArrayPadre[Cont2])
        for x in range(len(ArrayPadre[Cont2])):
                table.rows[cont+C3].cells[0].paragraphs[0].add_run(ArrayPadre[Cont2][x])
                table.rows[cont+C3].cells[0].paragraphs[0].alignment = 1
                
                cont=cont+1
        cont=cont-1    
        table.rows[MeIn].cells[1].paragraphs[0].add_run(ArrayPadre[Cont2+1][0]+" al "+ArrayPadre[Cont2+1][1])
        table.rows[MeIn].cells[1].paragraphs[0].alignment = 1
        table.cell(MeIn, 1).merge(table.cell(CantPro+MeIn-1,1))
        
        for x in range(len(ArrayPadre[Cont2+2])):
            dependencia_temporal = Dependencia.objects.get(nombre = ArrayPadre[Cont2+2][x])
            if dependencia_temporal.facultad:
                table.rows[MeIn].cells[2].paragraphs[0].add_run("*"+ArrayPadre[Cont2+2][x]+".")
            else:
                table.rows[MeIn].cells[2].paragraphs[0].add_run(ArrayPadre[Cont2+2][x]+".")

            table.rows[MeIn].cells[2].paragraphs[0].add_run("\n")
            table.rows[MeIn].cells[2].paragraphs[0].alignment = 1
        table.cell(MeIn, 2).merge(table.cell(CantPro+MeIn-1,2))

        MeIn=MeIn+CantPro;
        C3=C3+1
        Cont2=Cont2+3
        y=y+3

    Comentarios= "* Facultades comprendidas en el Plan de Auditoría\n" + lista_grupo

    Elaborado = programa.auditor_elaborado.usuario.user.first_name + " " + programa.auditor_elaborado.usuario.user.last_name
    Revisado = programa.auditor_revisado.usuario.user.first_name + " " + programa.auditor_elaborado.usuario.user.last_name
    Aprobado = programa.auditor_aprobado.usuario.user.first_name + " " + programa.auditor_elaborado.usuario.user.last_name
    #Revisado="Mg. Daisy Gonzales Diaz – Jefe de Oficina Universitaria de Calidad"
    #Aprobado="Mg. Daisy Gonzales Diaz – Jefe de Oficina Universitaria de Calidad"

    table.rows[filas-2].cells[0].paragraphs[0].add_run(Comentarios)
    table.cell(filas-2, 0).merge(table.cell(filas-2,2))
    table.rows[filas-1].cells[0].paragraphs[0].add_run("Elaborado por: "+Elaborado+"\n"+"Revisado por: "+Revisado+"\n"+"Aprobado por: "+Aprobado+"\n")
    
    if programa.firma != "":
        set_cell_margins(table.rows[filas-1].cells[0], bottom = 80)
        table.rows[filas-1].cells[0].paragraphs[0].add_run("\nFirma:\n")
        table.rows[filas-1].cells[0].paragraphs[0].add_run().add_picture(ruta_firma, width=Inches(1.3), height=Inches(0.8))
    table.cell(filas-1, 0).merge(table.cell(filas-1,2))

    rutaDocx = "static/pdfs/programa-" + str(programa.id) + programa.fecha_modificacion.strftime("%Y%m%d%H%M%S") + ".docx"
    document.save(rutaDocx)
    #convert(rutaDocx) # convierte el documento a pdf
    #remove(rutaDocx)ymdhm
    #convert("plan.docx", "D:/plan.pdf")

    #word = BytesIO()
    #document.save(word)
    #word.seek(0)
    #return word

def crearPdfPlan(id_plan):
    plan = PlanAuditoria.objects.get(id=id_plan)
    programa = ProgramaAuditoria.objects.get(id=plan.programa_auditoria_id)
    auditores_plan = list(PlanAuditores.objects.filter(plan_auditoria=plan))
    funciones_plan = list(PlanFunciones.objects.filter(plan_auditoria=plan))

    lista_padre = []

    bloques_procesos = list(BloqueProcesoPlan.objects.filter(
            plan_auditoria=plan))

    procesos_planes_lista = []
    for bloque_proceso in bloques_procesos:
        #procesos_planes_lista += list(ProcesoPlanAuditoria.objects.filter(bloque_proceso_auditoria=bloque_proceso))

        procesos_planes_temporal = list(ProcesoPlanAuditoria.objects.filter(bloque_proceso_auditoria=bloque_proceso))

        lista_padre += [[bloque_proceso, []]]

        for proceso_plan in procesos_planes_temporal:
            actividades_temporal = list(ActividadesProcesoPrograma.objects.filter(proceso_programa = proceso_plan.proceso_programa))
            dependencias_temporal = list(DependenciasProcesoPrograma.objects.filter(proceso_programa = proceso_plan.proceso_programa))
            equipo_temporal = list(EquipoProcesoPlan.objects.filter(proceso_plan_auditoria = proceso_plan))
            clausulas_temporal = list(ClausulaProcesoPlan.objects.filter(proceso_plan_auditoria = proceso_plan))

            equipo_procesado = []
            for equipo in equipo_temporal:
                equipo_procesado += [equipo.plan_auditor]

            proceso_temporal = [proceso_plan, actividades_temporal, dependencias_temporal, equipo_procesado, clausulas_temporal]
            lista_padre[len(lista_padre)-1][1] += [proceso_temporal]

    #print("Padre", lista_padre)

    #Función para activar bold en todas las celdas de las filas asignadas (fila 1, fila 2)
    def make_rows_bold(*rows):
        for row in rows:
            for cell in row.cells:
                for paragraph in cell.paragraphs:
                    for run in paragraph.runs:
                        run.font.bold = True
    #Función para volver bold la primera celda de las filas asignadas
    def make_justrows_bold(*rows):
            for row in rows:
                for paragraph in row.cells[0].paragraphs:
                    for run in paragraph.runs:
                        run.font.bold = True
    # Creación del documento word
    document = Document()
    
    
    style = document.styles['Normal']
    font = style.font
    font.name = "Arial"
    font.size = Pt(10)
    # Agragado de elemento al documento word
    section = document.sections[0]
    section.orientation = WD_ORIENT.LANDSCAPE
    section.page_width = Mm(280)  # for A4 paper
    section.page_height = Mm(215)
    #Margen de página
    section.top_margin = Cm(0.5)
    section.bottom_margin = Cm(0.5)
    section.left_margin = Cm(1)
    section.right_margin = Cm(1)
    header = section.header
    section.header_distance=Cm(0.5)

    #document BOLD
    #p=document.add_paragraph('El contenido de los párrafos se añadir en varias líneas. ')
    #p.add_run("BOLD").bold = True

    # Para enlistar document.add_paragraph('aa', style="List Bullet")

    #Tabla de Cabecera

    #Saltar pag document.add_page_break()

    table=header.add_table(rows=1,cols=4,width=130000)
    table.alignment = WD_TABLE_ALIGNMENT.CENTER
    table.style = 'TableGrid'
    
    #De este modo se puede agregar una imagen a una celda
    cells=table.add_row().cells
    paragraph=cells[0].paragraphs[0]
    run=paragraph.add_run()
    run.add_picture("static/images/unsa.png", width=Inches(1.6), height=Inches(0.6))

    table.rows[0].cells[1].text="Formato"
    table.rows[0].cells[1].width=Inches(3)
    table.rows[0].cells[1].paragraphs[0].alignment=1
    table.rows[0].cells[2].text="Código"
    table.rows[0].cells[2].width=Inches(0.8)
    table.rows[0].cells[3].text=plan.codigo #Variable de Codigo
    table.rows[0].cells[3].paragraphs[0].alignment=1
    table.rows[0].cells[3].width=Inches(1.5)
    #Variable de código
    
    cells[1].text="Plan de Auditoría"
    table.rows[1].cells[1].paragraphs[0].alignment=1
    cells[2].text="Versión"
    cells[3].text=plan.version #Variable de Versión
    table.rows[1].cells[3].paragraphs[0].alignment=1
    
    cells=table.add_row().cells

    #Margenes para la imagen luego de crear las filas
    set_cell_margins(table.rows[0].cells[0], top=100, start=100, end=100)
    set_cell_margins(table.rows[1].cells[0], top=100, bottom=100, end=100)

    cells[2].text="Revisión"
    cells[3].text=str(plan.n_revision) #De momento no sé como calcular solo cuantas pag tendrá
    table.rows[2].cells[3].paragraphs[0].alignment=1
    table.rows[1].cells[1]
    #De este modo se puede unir celdas
    table.cell(0, 0).merge(table.cell(2,0))
    table.cell(1, 1).merge(table.cell(2,1))
    header.add_paragraph('')
    

    #Tabla Cuerpo

    #Variables para for
    cantaud=len(auditores_plan) #Variable cantidad auditores
    columnas=7+cantaud
    criterios=3
    especialistas=2
    observadores=2
    facultad=2
    escuela=2
    direccionI=2
    direccionC=2


    table=document.add_table(rows=4,cols=columnas,style="Table Grid")
    
    table.rows[0].cells[0].paragraphs[0].add_run("N° de Auditoría")
    table.cell(0, 0).merge(table.cell(0,1))
    table.rows[0].cells[2].paragraphs[0].add_run(str(plan.n_auditoria)) #Varoable N auditoria
    table.rows[0].cells[2].paragraphs[0].alignment=1
    table.cell(0, 2).merge(table.cell(0,4))
    table.rows[0].cells[5].paragraphs[0].add_run("Tipo de Auditoría")
    table.cell(0, 5).merge(table.cell(0,6))
    make_rows_bold(table.rows[0])
    table.rows[0].cells[7].text=plan.tipo_auditoria #Varoable Tipo auditoria
    table.rows[0].cells[7].paragraphs[0].alignment=1
    table.cell(0, 7).merge(table.cell(0,columnas-1))
    
    table.rows[1].cells[0].paragraphs[0].add_run("Auditor Líder")
    table.cell(1, 0).merge(table.cell(1,1))
    texto=table.rows[1].cells[2].paragraphs[0].add_run(plan.auditor_lider.usuario.user.first_name + " " + plan.auditor_lider.usuario.user.last_name) #Nombre Auditor Lider)
    table.rows[1].cells[2].paragraphs[0].alignment=1
    table.cell(1, 2).merge(table.cell(1,columnas-1))
    table.rows[2].cells[0].text="Función y responsabilidad del Auditor líder:"
    table.cell(2, 0).merge(table.cell(2,1))
    table.rows[2].cells[2].text= plan.funcion_lider #Funcion
    table.rows[2].cells[2].paragraphs[0].alignment=1
    table.cell(2, 2).merge(table.cell(2,columnas-1))
    
    table.rows[3].cells[0].text="Fecha inicio de auditoría"

    table.cell(3, 0).merge(table.cell(3,2))
    table.rows[3].cells[3].text=plan.fecha_inicio.strftime('%d.%m.%Y') #F inicio
    table.rows[3].cells[3].paragraphs[0].alignment=1
    table.cell(3, 3).merge(table.cell(3,4))
    #Forma de personalmente convertir en Bold alguna celda
    texto= table.rows[3].cells[5].paragraphs[0].add_run("Fecha fin de auditoría")
    texto.bold=True
    table.cell(3, 5).merge(table.cell(3,6))
    table.rows[3].cells[7].text=plan.fecha_fin.strftime('%d.%m.%Y') #F fin
    table.rows[3].cells[7].paragraphs[0].alignment=1
    table.cell(3, 7).merge(table.cell(3,columnas-1))
    make_justrows_bold(table.rows[0],table.rows[1] ,table.rows[2],table.rows[3])
    
    #Auditores
    
    for i, auditor in zip(range(len(auditores_plan)), auditores_plan):
        cells=table.add_row().cells
        cells[2].text="A"+str(i+1)
        cells[3].text=auditor.auditor.usuario.user.first_name + " " + auditor.auditor.usuario.user.last_name
        table.cell(4+i, 3).merge(table.cell(4+i,4))
        cells[5].text=inicialesNombre(auditor.auditor.usuario.user.first_name + " " + auditor.auditor.usuario.user.last_name)

    texto=table.rows[4].cells[0].paragraphs[0].add_run("Auditor (es):")
    texto.bold=True
    
    table.cell(4, 0).merge(table.cell(4+i,1))
    texto=table.rows[4].cells[6].paragraphs[0].add_run("Función del equipo auditor:")
    texto.bold=True
    table.cell(4, 6).merge(table.cell(4+i,6))
    funcion_procesado = ""
    for funcion in funciones_plan:
        funcion_procesado += "- " + funcion.funcion + "\n"
    table.rows[4].cells[7].text=funcion_procesado 
    table.cell(4, 7).merge(table.cell(4+i,columnas-1))
    
    colActual=4+cantaud

    listaCriterios = listaOraciones(plan.programa_auditoria.criterios_auditoria)

    #Criterios
    for i in range(len(listaCriterios)):
        cells=table.add_row().cells
        cells[2].text="C"+str(i+1)
        cells[3].text=listaCriterios[i]
        table.cell(colActual+i, 3).merge(table.cell(colActual+i,columnas-1))

    texto=table.rows[colActual].cells[0].paragraphs[0].add_run("Criterios de auditoría")
    texto.bold=True
    table.cell(colActual, 0).merge(table.cell(colActual+i,1))

    colActual=colActual+len(listaCriterios)        

    #Especialistas
    for i in range(especialistas):
        
        cells=table.add_row().cells
        cells[2].text="E"+str(i+1)
        cells[3].text="N.A."
        table.cell(colActual+i, 3).merge(table.cell(colActual+i,columnas-1))
    texto=table.rows[colActual].cells[0].paragraphs[0].add_run("Especialista(s):")
    texto.bold=True
    table.cell(colActual, 0).merge(table.cell(colActual+i,1))

    colActual=colActual+especialistas
    

    #Alcance
    table.add_row().cells
    texto=table.rows[colActual].cells[0].paragraphs[0].add_run("Alcance")
    texto.bold=True
    table.cell(colActual, 0).merge(table.cell(colActual,1))
    texto=table.rows[colActual].cells[2].paragraphs[0].add_run(programa.alcance)
    table.cell(colActual, 2).merge(table.cell(colActual,columnas-1))

    #Requisitos no aplicables
    table.add_row().cells
    texto=table.rows[colActual+1].cells[0].paragraphs[0].add_run("Requisitos no aplicables")
    texto.bold=True
    table.cell(colActual+1, 0).merge(table.cell(colActual+1,2))
    texto=table.rows[colActual+1].cells[3].paragraphs[0].add_run("N.A.")
    table.cell(colActual+1, 3).merge(table.cell(colActual+1,columnas-1))
    colActual=colActual+2
    
    #Observadores
    for i in range(observadores):
        
        cells=table.add_row().cells
        cells[2].text="O"+str(i+1)
        cells[3].text="N.A."
        table.cell(colActual+i, 3).merge(table.cell(colActual+i,columnas-1))
    texto=table.rows[colActual].cells[0].paragraphs[0].add_run("Observador(es)")
    texto.bold=True
    table.cell(colActual, 0).merge(table.cell(colActual+i,1))

    colActual=colActual+observadores

    #Objetivos de la Auditoria
    table.add_row().cells
    texto=table.rows[colActual].cells[0].paragraphs[0].add_run("Objetivos de la Auditoria")
    texto.bold=True
    table.cell(colActual, 0).merge(table.cell(colActual,2))
    texto=table.rows[colActual].cells[3].paragraphs[0].add_run(programa.objetivo)
    table.cell(colActual, 3).merge(table.cell(colActual,columnas-1))
    colActual=colActual+1

    table.add_row().cells
    table.cell(colActual, 0).merge(table.cell(colActual,1))
    texto=table.rows[colActual].cells[2].paragraphs[0].add_run("Plan de Auditoria")
    table.cell(colActual, 2).merge(table.cell(colActual,columnas-1))
    texto.bold=True
    colActual=colActual+1
    #Comienzo de la siguiente Tabla
    table.add_row().cells
    table.rows[colActual].cells[0].paragraphs[0].add_run("Fecha")
    table.rows[colActual].cells[0].paragraphs[0].alignment=1
    table.rows[colActual].cells[1].paragraphs[0].add_run("Hora")
    table.rows[colActual].cells[1].paragraphs[0].alignment=1
    table.rows[colActual].cells[2].paragraphs[0].add_run("Lugar/Sitio")
    table.rows[colActual].cells[2].paragraphs[0].alignment=1
    table.rows[colActual].cells[3].paragraphs[0].add_run("Actividades")
    table.rows[colActual].cells[3].paragraphs[0].alignment=1
    table.rows[colActual].cells[4].paragraphs[0].add_run("Procesos/Clausulas")
    table.rows[colActual].cells[4].paragraphs[0].alignment=1
    
    table.rows[colActual].cells[6].paragraphs[0].add_run("Participantes/Auditado")
    table.rows[colActual].cells[6].paragraphs[0].alignment=1
    table.rows[colActual].cells[7].paragraphs[0].add_run("Equipo Auditor")
    table.rows[colActual].cells[7].paragraphs[0].alignment=1
    table.cell(colActual, 7).merge(table.cell(colActual,columnas-1))

    table.add_row().cells
    table.add_row().cells
    for i, auditor in zip(range(cantaud), auditores_plan):
        table.rows[colActual+1].cells[7+i].paragraphs[0].add_run("A"+str(i+1))
        table.rows[colActual+1].cells[7+i].paragraphs[0].alignment=1
        table.rows[colActual+2].cells[7+i].paragraphs[0].add_run(inicialesNombre(auditor.auditor.usuario.user.first_name + " " + auditor.auditor.usuario.user.last_name))
        table.rows[colActual+2].cells[7+i].paragraphs[0].alignment=1
    
    make_rows_bold(table.rows[colActual],table.rows[colActual+1])
    

    table.cell(colActual, 1).merge(table.cell(colActual+2,1))
    table.cell(colActual, 2).merge(table.cell(colActual+2,2))
    table.cell(colActual, 3).merge(table.cell(colActual+2,3))
    table.cell(colActual, 4).merge(table.cell(colActual+2,5))
    table.cell(colActual, 6).merge(table.cell(colActual+2,6))
    
    #Campo 1 y 2
    colActual=colActual+3
    
    #Generacion de bloques y procesos
    '''
    plan
    programa
    [auditores_plan]
    [funciones_plan]
    proceso_temporal = [proceso_plan, actividades_temporal, dependencias_temporal, equipo_procesado, clausulas_temporal]
    '''

    for indexPrueba, bloqueArray in zip(range(len(lista_padre)), lista_padre):
        table.add_row().cells
        table.rows[colActual].cells[0].paragraphs[0].add_run(bloqueArray[0].titulo)
        table.rows[colActual].cells[0].paragraphs[0].alignment=1
        table.cell(colActual, 0).merge(table.cell(colActual,6))
        for i in range(cantaud):
            table.rows[colActual].cells[7+i].paragraphs[0].add_run("A"+str(i+1))
            table.rows[colActual].cells[7+i].paragraphs[0].alignment=1
        make_rows_bold(table.rows[colActual])

        #For de Campos
        #colActual = colActual + 1
        for i, procesoArray in zip(range(len(bloqueArray[1])), bloqueArray[1]):
            #print("Table: ", table.height)
            table.add_row().cells
            colActual += 1
            #print("colActual: ", str(colActual))
            table.rows[colActual].cells[0].paragraphs[0].add_run(procesoArray[0].proceso_programa.fecha_inicio.strftime('%d.%m.%Y'))
            table.rows[colActual].cells[0].paragraphs[0].alignment=1
            table.rows[colActual].cells[1].paragraphs[0].add_run(procesoArray[0].hora_inicio.strftime('%H:%M') + " - " + procesoArray[0].hora_fin.strftime('%H:%M'))
            table.rows[colActual].cells[1].paragraphs[0].alignment=1
            table.rows[colActual].cells[2].paragraphs[0].add_run(procesoArray[0].lugar)
            table.rows[colActual].cells[2].paragraphs[0].alignment=1
            
            table.rows[colActual].cells[3].paragraphs[0].add_run(procesoArray[0].actividad) #Variable
            table.rows[colActual].cells[3].paragraphs[0].alignment=1
            
            procesos_cadena = "Procesos: "
            for proceso in procesoArray[1]:
                procesos_cadena += "\n-" + proceso.proceso.nombre

            if len(procesoArray[4]) !=0:
                procesos_cadena += "\n\nClausulas: "
                for clausula in procesoArray[4]:
                    procesos_cadena += clausula.clausula.nombre + ", "

            texto= table.rows[colActual].cells[4].text= procesos_cadena #Variable
            table.rows[colActual].cells[4].paragraphs[0].alignment=1

            dependencias_cadena = ""
            for dependencia in procesoArray[2]:
                dependencias_cadena += dependencia.dependencia.nombre + "\n"
            #No sé como enlistar
            table.cell(colActual, 4).merge(table.cell(colActual,5))
            table.rows[colActual].cells[6].paragraphs[0].add_run(dependencias_cadena)
            table.rows[colActual].cells[6].paragraphs[0].alignment=1

            #For que marca a todos, poner ifs dependiendo el caso
            for j, auditor in zip(range(cantaud), auditores_plan):
                if auditor in procesoArray[3]:
                    table.rows[colActual].cells[7+j].paragraphs[0].add_run("X")
                else:
                    table.rows[colActual].cells[7+j].paragraphs[0].add_run(" ")
                table.rows[colActual].cells[7+j].paragraphs[0].alignment=1

        colActual+=1

    rutaDocx = "static/pdfs/plan-" + str(plan.id) + plan.fecha_modificacion.strftime("%Y%m%d%H%M%S") + ".docx"
    document.save(rutaDocx)
    #convert(rutaPlan) # convierte el documento a pdf
    #remove(rutaPlan)
    #convert("plan.docx", "D:/plan.pdf")

    #word = BytesIO()
    #document.save(word)
    #word.seek(0)
    #return word