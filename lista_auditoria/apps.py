from django.apps import AppConfig


class ListaAuditoriaConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'lista_auditoria'
