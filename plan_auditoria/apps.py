from django.apps import AppConfig


class PlanAuditoriaConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'plan_auditoria'
