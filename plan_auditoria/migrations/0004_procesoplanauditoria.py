# Generated by Django 4.0.5 on 2022-12-11 19:43

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('plan_auditoria', '0003_planauditoria_funcion_lider'),
    ]

    operations = [
        migrations.CreateModel(
            name='ProcesoPlanAuditoria',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('lugar', models.CharField(max_length=100)),
                ('plan_auditoria', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='plan_auditoria.planauditoria')),
            ],
        ),
    ]
