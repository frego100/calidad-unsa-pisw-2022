# Generated by Django 4.0.5 on 2023-02-04 18:22

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('plan_auditoria', '0018_rename_pdf_generado_planauditoria_docx_generado'),
    ]

    operations = [
        migrations.AddField(
            model_name='planauditoria',
            name='fecha_modificacion',
            field=models.DateTimeField(blank=True, default=datetime.datetime.now),
        ),
    ]
