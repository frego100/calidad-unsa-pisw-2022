from django.db import models
from programa_auditoria.models import ProgramaAuditoria
from programa_auditoria.models import ProcesoPrograma
from usuarios_auditoria.models import Auditor
from administracion.models import Clausula, Facultad, Escuela
import datetime
from django.utils.timezone import now
from datetime import datetime as fulltime

# Create your models here.
class PlanAuditoria(models.Model):
    auditor_lider = models.ForeignKey(Auditor,null=True,blank=True,on_delete=models.CASCADE,related_name='auditor_lider')
    funcion_lider = models.CharField(max_length=100, default="Gestionar la auditoría y elaborar el informe de auditoría")
    programa_auditoria = models.ForeignKey(ProgramaAuditoria,null=True,blank=True,on_delete=models.CASCADE)
    titulo = models.CharField(max_length=100, default="")
    fecha_inicio = models.DateTimeField(default=now, blank=True)
    fecha_fin = models.DateTimeField(default=now, blank=True)
    tipo_auditoria = models.CharField(max_length=100, default="Interna")
    n_auditoria = models.IntegerField(default=1)
    n_revision = models.IntegerField(default=1)
    version = models.CharField(max_length=100, default="2.0")
    codigo = models.CharField(max_length=100, default="F-PE01.04-07")
    n_revision = models.IntegerField(default=1)
    docx_generado = models.BooleanField(default=False)
    fecha_modificacion = models.DateTimeField(default=fulltime.now, blank=True)
    
class PlanAuditores(models.Model):
    plan_auditoria = models.ForeignKey(PlanAuditoria,null=True,blank=True,on_delete=models.CASCADE)
    auditor = models.ForeignKey(Auditor,null=True,blank=True,on_delete=models.CASCADE)

class PlanFunciones(models.Model):
    plan_auditoria = models.ForeignKey(PlanAuditoria,null=True,blank=True,on_delete=models.CASCADE)
    funcion = models.CharField(max_length=100)
    
class BloqueProcesoPlan(models.Model):
    plan_auditoria = models.ForeignKey(PlanAuditoria,null=True,blank=True,on_delete=models.CASCADE)
    facultad =  models.ForeignKey(Facultad,null=True,blank=True,on_delete=models.CASCADE)
    escuela =  models.ForeignKey(Escuela,null=True,blank=True,on_delete=models.CASCADE)
    titulo = models.CharField(max_length=100)
        
class ProcesoPlanAuditoria(models.Model):
    bloque_proceso_auditoria = models.ForeignKey(BloqueProcesoPlan,null=True,blank=True,on_delete=models.CASCADE)
    proceso_programa = models.ForeignKey(ProcesoPrograma,null=True,blank=True,on_delete=models.CASCADE)
    lugar = models.CharField(max_length=100, default="")
    actividad = models.CharField(max_length=100,default="")
    hora_inicio = models.TimeField(default=datetime.time(00, 00))
    hora_fin = models.TimeField(default=datetime.time(00, 00))
    fecha = models.DateTimeField(default=now, blank=True)

class ClausulaProcesoPlan(models.Model):
    proceso_plan_auditoria = models.ForeignKey(ProcesoPlanAuditoria,null=True,blank=True,on_delete=models.CASCADE)
    clausula = models.ForeignKey(Clausula,null=True,blank=True,on_delete=models.CASCADE)

class EquipoProcesoPlan(models.Model):
    proceso_plan_auditoria = models.ForeignKey(ProcesoPlanAuditoria,null=True,blank=True,on_delete=models.CASCADE)
    plan_auditor = models.ForeignKey(PlanAuditores,null=True,blank=True,on_delete=models.CASCADE)
   
