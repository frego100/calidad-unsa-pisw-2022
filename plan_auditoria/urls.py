from django.urls import path, re_path
from . import views

urlpatterns = [
        #path("add", views.SavePlan.as_view(), name='add-plan'),
        #path("edit", views.EditPlan.as_view(), name='edit-plan'),
        re_path(r'edit/(?P<data_id>[0-9]+)$',
            views.EditPlan.as_view(), name='edit-plan'),
        path("view", views.ListPlan, name="view-plan"),
        path('viewPdf/<int:data_id>', views.ViewPdfPlan, name='view-pdf-plan'),
        ]
