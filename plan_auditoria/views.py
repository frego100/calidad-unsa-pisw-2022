from django.shortcuts import render, redirect
from django.views import View
from django.http import HttpResponse
from programa_auditoria.models import ProgramaAuditoria, ProcesoPrograma, ActividadesProcesoPrograma, DependenciasProcesoPrograma
from usuarios_auditoria.models import Usuario, Auditor
from administracion.models import Clausula
from .models import PlanAuditoria, BloqueProcesoPlan, ProcesoPlanAuditoria, PlanAuditores, PlanFunciones, ClausulaProcesoPlan, EquipoProcesoPlan
from generacion_pdfs.views import crearPdfPrograma, crearPdfPlan, generarPlanesPdfs
from os import remove
from datetime import datetime
import os
# Create your views here.


def ViewPdfPlan(request, data_id):
    auth_usuario = request.user
    context = {"id": data_id}
    if request.user.is_authenticated:
        usuario = Usuario.objects.get(user=auth_usuario)
        context["user"] = usuario

    return render(request, "plan-view-pdf.html", context)


def DeletePlan(plan):
    try:
        rutaDocx = "static/pdfs/plan-" + str(plan.id) + plan.fecha_modificacion.strftime("%Y%m%d%H%M%S") + ".docx"
        remove(rutaDocx)
    except:
        print("Archivo no encontrado")
    

def ListPlan(request):
    if not request.user.is_authenticated:
        return  redirect("login")
    
    auth_usuario = request.user
    planes = PlanAuditoria.objects.all()

    context = {"planes": planes}
    if request.user.is_authenticated:
        usuario = Usuario.objects.get(user=auth_usuario)
        context["user"] = usuario

    return render(request, "plan-table.html", context)


class EditPlan(View):
    def get(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return  redirect("login")   
        if request.user.is_authenticated and Usuario.objects.get(user=request.user).rol != "administrador":
            return  redirect("principal")

        auditores = Auditor.objects.filter(estado = True)
        clausulas = Clausula.objects.all()

        plan = PlanAuditoria.objects.get(id=self.kwargs['data_id'])

        auditores_plan = PlanAuditores.objects.filter(plan_auditoria=plan)
        funciones_plan = PlanFunciones.objects.filter(plan_auditoria=plan)

        bloques_procesos = BloqueProcesoPlan.objects.filter(
            plan_auditoria=plan)

        procesos_planes_lista = []
        for bloque_proceso in bloques_procesos:
            procesos_planes_lista += list(ProcesoPlanAuditoria.objects.filter(
                bloque_proceso_auditoria=bloque_proceso))

        #print("Procesos planes lista: ", procesos_planes_lista)

        #procesos_planes = [[proceso_plan, proceso_programa, [actvidades], dependencia]]
        proceso_planes_procesado = []
        for proceso_plan in procesos_planes_lista:

            clausulas_proceso_temporal = ClausulaProcesoPlan.objects.filter(proceso_plan_auditoria = proceso_plan)
            clausulas_procesado = []
            for cla in clausulas_proceso_temporal:
                clausulas_procesado += [ Clausula.objects.get(id  = cla.clausula_id)  ]

            equipos = EquipoProcesoPlan.objects.filter(proceso_plan_auditoria = proceso_plan)
            equipos_proceso = []
            for equipo in equipos:
                equipos_proceso += [ PlanAuditores.objects.get(id = equipo.plan_auditor.id) ]

            proceso_programa_temporal = ProcesoPrograma.objects.get(
                id=proceso_plan.proceso_programa_id)
            proceso_planes_procesado += [[proceso_plan, proceso_programa_temporal, list(
                ActividadesProcesoPrograma.objects.filter(proceso_programa=proceso_programa_temporal)), DependenciasProcesoPrograma.objects.get(proceso_programa=proceso_programa_temporal), clausulas_procesado, equipos_proceso, equipos]]

        context = {
            "plan": plan,
            "auditores_plan": auditores_plan,
            "funciones_plan": funciones_plan,
            "bloques_procesos": bloques_procesos,
            "procesos_planes": proceso_planes_procesado,
            "auditores": auditores,
            "clausulas": clausulas
        }

        if request.user.is_authenticated and Usuario.objects.get(user=request.user).rol == "administrador":
            usuario = Usuario.objects.get(user=request.user)
            context["user"] = usuario
            return render(request, "plan-edit.html", context)

        return redirect(ListPlan)

    def post(self, request,*args, **kwargs):
        if not request.user.is_authenticated:
            return  redirect("login")   
        if request.user.is_authenticated and Usuario.objects.get(user=request.user).rol != "administrador":
            return  redirect("principal")
        
        try:
            plan = PlanAuditoria.objects.get(id = self.kwargs['data_id'])

            antigua_fecha_plan = plan.fecha_modificacion

            plan.titulo = request.POST.get("titulo_plan")
            #n_revision
            plan.n_revision = request.POST.get("n_revision")
            #n_auditoria
            plan.n_auditoria = request.POST.get("n_auditoria")
            #tipo_auditoria (INTERNA EXTERNA)
            plan.tipo_auditoria = request.POST.get("tipo_auditoria")
            #auditor_lider (ID)
            plan.auditor_lider = Auditor.objects.get(id= request.POST.get("auditor_lider"))
            #funcion_lider
            plan.funcion_lider = request.POST.get("funcion_lider")
            #fecha_inicio
            #plan.fecha_inicio = request.POST.get("fecha_inicio")
            #fecha_fin
            #plan.fecha_fin = request.POST.get("fecha_fin") 
            #fecha_modificacion
            plan.fecha_modificacion = datetime.now()

            #ACTUALIZAR O ELIMINAR ANTIGUOS AUDITORES
            antiguo_auditor_id = request.POST.getlist("antiguo_auditor_id")
            antiguo_auditor_status = request.POST.getlist("antiguo_auditor_status")
            antiguo_auditor = request.POST.getlist("antiguo_auditor")

            for auditorPlanId, auditorPlanStatus, auditorId in zip(antiguo_auditor_id, antiguo_auditor_status, antiguo_auditor):

                plan_auditor = PlanAuditores.objects.get( id  = auditorPlanId)
                if auditorPlanStatus == "0":
                    plan_auditor.delete()
                else :
                    plan_auditor.auditor = Auditor.objects.get ( id  = auditorId)
                    plan_auditor.save()
            
            #Fechas_procesos_plan
            fechas_procesos_plan = request.POST.getlist("proceso_plan_fechas")

            #NUEVOS AUDITORES
            auditores = request.POST.getlist("auditores")
            print("Auditores 1  : ", auditores)

            for auditor in auditores:
                plan_auditor = PlanAuditores.objects.create(
                    plan_auditoria = plan,
                    auditor =  Auditor.objects.get ( id  = auditor)
                )
        
            
            #FUNCIONES

            #ACTUALIZAR O ELIMINAR ANTIGUOS Funciones
            antiguo_funcion_id = request.POST.getlist("antiguo_funcion_id")
            antiguo_funcion_status = request.POST.getlist("antiguo_funcion_status")
            antiguo_funcion = request.POST.getlist("antiguo_funcion")
            
            #print("antiguo_funcion_id", antiguo_funcion_id)
            #print("antiguo_funcion_status", antiguo_funcion_status)
            #print("antiguo_funcion", antiguo_funcion)

            for funcionPlanId, funcionPlanStatus, funcion in zip(antiguo_funcion_id, antiguo_funcion_status, antiguo_funcion):

                plan_funcion = PlanFunciones.objects.get( id  = funcionPlanId)
                if funcionPlanStatus == "0":
                    plan_funcion.delete()
                else :
                    plan_funcion.funcion = funcion
                    plan_funcion.save()
            
            #NUEVOS Funciones
            funciones = request.POST.getlist("funciones")
            for funcion in funciones:
                plan_funcion = PlanFunciones.objects.create(
                    plan_auditoria = plan,
                    funcion = funcion
                )
            
            # BLOQUES 

            lugares = request.POST.getlist("lugares")
            actividades = request.POST.getlist("actividades")
            titulos = request.POST.getlist("titulos")
            #titulos
            bloques = list(BloqueProcesoPlan.objects.filter(plan_auditoria = plan))
            #aux_lugar =0
            for index_bloques, titulo in zip(range(len(bloques)), titulos):
                bloque = bloques[index_bloques]
                bloque.titulo = titulo
                bloque.save()
            
            lista_procesos = []
            for bloque in bloques:
                lista_procesos += list(ProcesoPlanAuditoria.objects.filter(bloque_proceso_auditoria= bloque ))
            
            #print("Lista procesos ",lista_procesos)
            
            for index, proceso in zip(range(len(lista_procesos)), lista_procesos):
                fechas = request.POST.getlist(f'proceso_{index+1}_fechas')
                clausulas = request.POST.getlist(f'clausulas_{index+1}')
                proceso.hora_inicio = fechas[0]
                proceso.hora_fin = fechas[1]
                proceso.lugar = lugares[index]
                proceso.actividad = actividades[index]
                proceso.fecha = fechas_procesos_plan[index]

                # CLAUSULAS
                clausulas_proceso = list(ClausulaProcesoPlan.objects.filter( proceso_plan_auditoria = proceso))
                clausulas_antiguas = []
                
                for clausula_pro in clausulas_proceso:
                    clausulas_antiguas += [str(clausula_pro.clausula.id)]

                eliminar = set(clausulas_antiguas) - set(clausulas)
                crear = set(clausulas) - set(clausulas_antiguas)

                for delete in eliminar:
                    cla = Clausula.objects.get(id = delete )
                    clausula_delete = ClausulaProcesoPlan.objects.get( proceso_plan_auditoria = proceso , clausula =cla)
                    clausula_delete.delete()

                for add in crear:
                    #cla = Clausula.objects.get(id = add )
                    clausula_crear = ClausulaProcesoPlan.objects.create(
                        proceso_plan_auditoria = proceso,
                        clausula = Clausula.objects.get(id = add)
                    )
                
                #AUDITORES antiguos
                antiguo_auditores_checkbox = request.POST.getlist(f'antiguo_auditor_checkbox_{index+1}')
                antiguo_auditores_checkbox_status = request.POST.getlist(f'antiguo_auditor_checkbox_status_{index+1}')
                antiguo_auditores_checkbox_id = request.POST.getlist(f'antiguo_auditor_checkbox_id_{index+1}')
                
                #Auditores nuevos
                auditores_checkbox_position = request.POST.getlist(f'auditor_checkbox_position_{index+1}')
                auditores_checkbox = request.POST.getlist(f'auditor_checkbox_{index+1}')

                for index2 in range(len(antiguo_auditores_checkbox)):
                    if antiguo_auditores_checkbox_status[index2] == "0":
                        try:
                            equipoProcesoPlan =  EquipoProcesoPlan.objects.get ( id = antiguo_auditores_checkbox[index2])
                            equipoProcesoPlan.delete()
                        except:
                            print("Plan auditor no encontrado o borrado")
                    elif antiguo_auditores_checkbox_status[index2] == "1":
                        try:
                            equipoProcesoPlan = EquipoProcesoPlan.objects.get (id = antiguo_auditores_checkbox[index2] ) 
                        except:
                            equipoProcesoPlan = None
                        
                        if equipoProcesoPlan == None:
                            try:
                                plan_auditor = PlanAuditores.objects.get(plan_auditoria=plan  ,auditor = Auditor.objects.get(id = int(antiguo_auditores_checkbox_id[index2])))
                            except:
                                plan_auditor = PlanAuditores.objects.get(plan_auditoria = plan, auditor = Auditor.objects.get(id = int(auditores_checkbox[index2])))
                            
                            equipoProcesoPlanCreate = EquipoProcesoPlan.objects.create (
                                proceso_plan_auditoria= proceso ,
                                plan_auditor= plan_auditor 
                            )
                            
                #AUDITORES nuevos
                

                print("auditores_checkbox_position", auditores_checkbox_position)
                print("auditores_checkbox", auditores_checkbox)

                auditoresPlan = PlanAuditores.objects.filter(plan_auditoria = plan)
                print("Auditores 2 : ", auditores)

                for index2 in range(len(auditores_checkbox_position)):
                    auditor_aux = Auditor.objects.get(id = int(auditores_checkbox[index2]))
                    plan_auditor_aux  = None
                    for aux in auditoresPlan :
                        if aux.auditor == auditor_aux:
                            plan_auditor_aux = aux

                    equipoProcesoPlanCreate = EquipoProcesoPlan.objects.create (
                                proceso_plan_auditoria= proceso ,
                                #plan_auditor= auditoresPlan[int(auditores_checkbox_position[index2])-1] 
                                plan_auditor = plan_auditor_aux
                            )


                #print("Crear", crear)
                #print("Eliminar", eliminar)
                    
                #print("Clausulas antiguas", clausulas_antiguas)
                #print("Clausulas marcadas", clausulas)

                proceso.save()
                

            #lugares
            #actividades
            #clausulas_[num]

            #antiguo_auditor_checbox_[num]
            #antiguo_auditor_checkbox_status_[num]

            #auditor_checkbox_[num]
            
            #plan.save()
        
        except:
            print("Generacion de plan")

        auditores = list(PlanAuditores.objects.filter(plan_auditoria = plan))

        if(len(auditores) >= 1) :
            try:
                rutaDocx = "static/pdfs/plan-" + str(plan.id) + antigua_fecha_plan.strftime("%Y%m%d%H%M%S") + ".docx"
                if os.path.exists(rutaDocx):
                    remove(rutaDocx)

                plan.docx_generado = True
                plan.save()
                crearPdfPlan(plan.id)
                    
            except Exception as error:
                print("Error al generar el pdf", error)
                plan.docx_generado = False
                plan.save()
            
        else:
            rutaDocx = "static/pdfs/plan-" + str(plan.id) + antigua_fecha_plan.strftime("%Y%m%d%H%M%S") + ".docx"
            try:
                plan.docx_generado = False
                plan.save()
                remove(rutaDocx)
            except:
                print("No existe el plan pdf con ese nombre")
            
        return redirect(ListPlan)

'''class SavePlan(View):
    def get(self, request):

        programas = ProgramaAuditoria.objects.all()
        auditores = Auditor.objects.all()
        context = {"programas": programas,
                   "auditores": auditores
                   }

        if request.user.is_authenticated and Usuario.objects.get(user=request.user).rol == "administrador":
            usuario = Usuario.objects.get(user=request.user)
            context["user"] = usuario
            return render(request, "plan-add.html", context)

        return HttpResponse('Vista Crear Plan')
        # return redirect(ListPrograma)

    def post(self, request):
        if request.method == 'POST':
            auditor_aprobado = request.POST.get("auditor_lider")
            programa_auditoria = request.POST.get("programa_auditoria")
            fecha_inicio = request.POST.get("fecha_inicio")
            fecha_fin = request.POST.get("fecha_fin")
            tipo_auditoria = request.POST.get("tipo_auditoria")
            n_auditoria = request.POST.get("n_auditoria")
            n_revision = request.POST.get("n_revision")
            version = request.POST.get("version")

            funciones = request.POST.getlist('funciones')
            auditores = request.POST.getlist('auditores')
            print(auditor_aprobado)
            print(funciones)
            print(auditores)
        return HttpResponse('Guardar Plan')'''
