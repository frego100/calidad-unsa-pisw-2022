from django.apps import AppConfig


class ProgramaAuditoriaConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'programa_auditoria'
