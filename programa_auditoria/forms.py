from django import forms
from programa_auditoria.models import ProgramaAuditoria
from usuarios_auditoria.models import Auditor
class ProgramaForm(forms.ModelForm):
    class Meta:
        model = ProgramaAuditoria
        fields = ['id', 'auditor_aprobado', 'auditor_elaborado', 'auditor_revisado', 'codigo', 'version', 'objetivo', 'alcance', 'recursos', 'control_riesgo', 'n_revision', 'firma']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['firma'].widget.clear_checkbox_label = 'Quitar firma'
        self.fields['firma'].widget.initial_text = "Imagen actual"
        self.fields['firma'].widget.input_text = "Selecciona una imagen para cambiar la firma"

    id = forms.IntegerField(label='id', required=False)
    auditor_aprobado = forms.ModelChoiceField(required=False, queryset=Auditor.objects.all(), empty_label="Selecciona un auditor")
    auditor_elaborado = forms.ModelChoiceField(queryset=Auditor.objects.all(), empty_label="Selecciona un auditor")
    auditor_revisado = forms.ModelChoiceField(required=False, queryset=Auditor.objects.all(), empty_label="Selecciona un auditor")
    codigo = forms.CharField(label='codigo')
    version = forms.CharField(label='version')
    objetivo = forms.CharField(label='objetivo')
    alcance = forms.CharField(label='alcance')
    recursos = forms.CharField(label='recursos')
    control_riesgo = forms.CharField(label='control_riesgo')
    n_revision = forms.IntegerField(label='n_revision')
    firma = forms.ImageField(label="firma", required=False)