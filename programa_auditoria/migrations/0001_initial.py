# Generated by Django 4.0.5 on 2022-11-12 22:09

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('usuarios_auditoria', '0002_auditor'),
    ]

    operations = [
        migrations.CreateModel(
            name='ProgramaAuditoria',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('codigo', models.CharField(max_length=100)),
                ('version', models.CharField(max_length=100)),
                ('objetivo', models.CharField(max_length=100)),
                ('alcance', models.CharField(max_length=100)),
                ('recursos', models.CharField(max_length=200)),
                ('control_riesgo', models.CharField(max_length=200)),
                ('n_revision', models.IntegerField(max_length=100)),
                ('firma', models.ImageField(null=True, upload_to='imagenes')),
                ('auditor_aprobado', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='auditor_aprobado', to='usuarios_auditoria.auditor')),
                ('auditor_elaborado', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='auditor_elaborado', to='usuarios_auditoria.auditor')),
                ('auditor_revisado', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='auditor_revisado', to='usuarios_auditoria.auditor')),
            ],
        ),
    ]
