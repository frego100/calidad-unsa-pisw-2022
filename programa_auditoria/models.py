from django.db import models
from usuarios_auditoria.models import Auditor
from administracion.models import Proceso, Dependencia
from datetime import datetime

# Create your models here.
class ProgramaAuditoria(models.Model):
    auditor_aprobado = models.ForeignKey(Auditor,null=True,blank=True,on_delete=models.CASCADE,related_name='auditor_aprobado')
    auditor_elaborado = models.ForeignKey(Auditor,null=True,blank=True,on_delete=models.CASCADE,related_name='auditor_elaborado')
    auditor_revisado = models.ForeignKey(Auditor,null=True,blank=True,on_delete=models.CASCADE,related_name='auditor_revisado')
    titulo = models.CharField(max_length=100)
    codigo = models.CharField(max_length=100, default="F-PE01.04-06")
    version = models.CharField(max_length=100, default="2.0")
    objetivo = models.CharField(max_length=100)
    alcance = models.CharField(max_length=100)
    criterios_auditoria = models.CharField(max_length=300, default="")
    modalidad = models.CharField(max_length=50, default="Virtual")
    lugar = models.CharField(max_length=150, default="")
    recursos = models.CharField(max_length=200)
    control_riesgo = models.CharField(max_length=200)
    n_revision = models.IntegerField()
    firma = models.ImageField(upload_to="imagenes/%Y/%m/%d/%H/%M/%S", null=True, blank=True)
    docx_generado = models.BooleanField(default=False)
    fecha_modificacion = models.DateTimeField(default=datetime.now, blank=True)

class ProcesoPrograma(models.Model):
    programa_auditoria = models.ForeignKey(ProgramaAuditoria,null=True,blank=True,on_delete=models.CASCADE)
    fecha_inicio = models.DateTimeField()
    fecha_fin = models.DateTimeField()
  

class ActividadesProcesoPrograma(models.Model):
    proceso_programa = models.ForeignKey(ProcesoPrograma,null=True,blank=True,on_delete=models.CASCADE)
    proceso = models.ForeignKey(Proceso,null=True,blank=True,on_delete=models.CASCADE)

    def __str__(self):
        return self.proceso.nombre

class DependenciasProcesoPrograma(models.Model):
    proceso_programa = models.ForeignKey(ProcesoPrograma,null=True,blank=True,on_delete=models.CASCADE)
    dependencia = models.ForeignKey(Dependencia,null=True,blank=True,on_delete=models.CASCADE)

    def __str__(self):
        return self.dependencia.nombre

#Obtenemos lista de dependencias
#Obtenemos lista de ProcesosPrograma de un Programa
#Iteramos lista de dependencias y en cada iteracion obtenemos lista de dependencias