from django.urls import path, re_path
from . import views

urlpatterns = [
    path("add", views.SavePrograma.as_view(), name='add-programa'),
    re_path(r'edit/(?P<data_id>[0-9]+)$',
            views.EditPrograma.as_view(), name='edit-programa'),
    path("view", views.ListPrograma, name="view-programa"),
    path('delete/<int:data_id>', views.DeletePrograma, name='delete-programa'),
    path('viewPdf/<int:data_id>', views.ViewPdfPrograma, name='view-pdf-programa'),
]
