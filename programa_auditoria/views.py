from django.shortcuts import render, redirect
from django.views import View
from django.views.generic.edit import UpdateView
from programa_auditoria.forms import ProgramaForm
from usuarios_auditoria.models import Auditor, Usuario
from plan_auditoria.models import PlanAuditoria, BloqueProcesoPlan, ProcesoPlanAuditoria, PlanAuditores, PlanFunciones
from programa_auditoria.models import ProgramaAuditoria, ProcesoPrograma,ActividadesProcesoPrograma,DependenciasProcesoPrograma
from administracion.models import Proceso, Dependencia, Facultad, Escuela
from django.http import JsonResponse
from os import remove
import os
from generacion_pdfs.views import crearPdfPrograma, crearPdfPlan, generarPlanesPdfs
from plan_auditoria.views import DeletePlan
from datetime import datetime

# Create your views here.

def ViewPdfPrograma(request, data_id):
    auth_usuario = request.user
    context = {"id": data_id}
    if request.user.is_authenticated:
        usuario = Usuario.objects.get(user=auth_usuario)
        context["user"] = usuario

    return render(request, "programa-view-pdf.html", context)

def DeletePrograma(_request, data_id):
    if not _request.user.is_authenticated:
        return  redirect("login")
    if _request.user.is_authenticated and Usuario.objects.get(user=_request.user).rol != "administrador":
        return  redirect("principal")

    try:
        programa = ProgramaAuditoria.objects.get(id = data_id)
        planes = PlanAuditoria.objects.filter(programa_auditoria = programa)
        for plan in planes:
            DeletePlan(plan)
            plan.delete()

        try:
            rutaDocx = "static/pdfs/programa-" + str(programa.id) + programa.fecha_modificacion.strftime("%Y%m%d%H%M%S") + ".docx"
            remove(rutaDocx)
        except:
            print("Archivo no encontrado")
        programa.delete()
        
        data = {'message': "Success"}
    except:
        data = {'message': "Not Found"}
    return redirect(ListPrograma)

def ListPrograma(request):
    if not request.user.is_authenticated:
        return  redirect("login")
    
    auth_usuario = request.user
    programas = ProgramaAuditoria.objects.all()
    context = {"programas": programas}
    if request.user.is_authenticated:
        usuario = Usuario.objects.get(user=auth_usuario)
        context["user"] = usuario

    return render(request, "programa-table.html", context)

class EditPrograma(View):
   
    #programa = None
    def get(self, request ,*args, **kwargs):
        if not request.user.is_authenticated:
            return  redirect("login")   
        if request.user.is_authenticated and Usuario.objects.get(user=request.user).rol != "administrador":
            return  redirect("principal")
        #print (self.kwargs['data_id'] )

        procesos = Proceso.objects.all()
        dependencias = Dependencia.objects.all()
        auditores = Auditor.objects.filter(estado = True)
        facultades = Facultad.objects.all()
        escuelas = Escuela.objects.all()

        programa = ProgramaAuditoria.objects.get(id=self.kwargs['data_id'])
        #programa = ProgramaAuditoria.objects.filter(codigo="a")
        
        procesos_lista = []
        #print(procesos_lista)

        procesos_programa = ProcesoPrograma.objects.filter(programa_auditoria=programa)
        #print (procesos_programa)
        #procesos_programa  = ProcesoPrograma.get.all(filter (programa_auditoria  = programa ))
        i = 0
        for procesos_unicos in procesos_programa:
            procesos_lista += [[0,0,0,[],[] ]] 
            print (procesos_unicos.fecha_inicio )
            #array [0][0]  = 1
            #array += [[2,3,[],[]]  ]
            procesos_lista[i][0] = procesos_unicos.id 
            procesos_lista[i][1] = procesos_unicos.fecha_inicio 
            procesos_lista[i][2] = procesos_unicos.fecha_fin 
           
            
            actividades = ActividadesProcesoPrograma.objects.filter(proceso_programa=procesos_unicos)
           
            for actividad in actividades:
                procesos_lista[i][3] += [[Proceso.objects.get(id = actividad.proceso.id) , actividad] ]
            
            dependencias_programa = DependenciasProcesoPrograma.objects.filter(proceso_programa=procesos_unicos)
            
            for dependencia_programa in dependencias_programa:
                procesos_lista[i][4] += [[Dependencia.objects.get(id = dependencia_programa.dependencia.id), dependencia_programa]] 
            
            i+=1
            
        #print (procesos_lista)
        planes = PlanAuditoria.objects.filter(programa_auditoria = programa )
        escuelas_antiguo =[]
        facultades_antiguo =[]
        for plan in planes :
            bloques = BloqueProcesoPlan.objects.filter( plan_auditoria = plan)

            for bloque in bloques :
                if bloque.escuela != None:
                    escuelas_antiguo += [bloque.escuela]
                elif bloque.facultad != None:
                     facultades_antiguo += [bloque.facultad]

        print(escuelas_antiguo)
        print(facultades_antiguo)
        
        context = {
            'procesos': procesos,
            'dependencias': dependencias,
            'auditores': auditores,
            'programa': programa,
            'procesos_lista': procesos_lista,
            'facultades': facultades,
            'escuelas': escuelas,
            'facultades_antiguo': facultades_antiguo,
            'escuelas_antiguo': escuelas_antiguo
        }


        if request.user.is_authenticated and Usuario.objects.get(user=request.user).rol == "administrador":
            usuario = Usuario.objects.get(user=request.user)
            context["user"] = usuario
            return render(request, "programa-edit.html", context)
        
        #print(programa)
        #get_context_data or get_object.

        #return render(request, "programa-add.html")

        return redirect(ListPrograma)

    def post(self, request ,*args, **kwargs):
        if not request.user.is_authenticated:
            return  redirect("login")   
        if request.user.is_authenticated and Usuario.objects.get(user=request.user).rol != "administrador":
            return  redirect("principal")

        try:
            #print (self.kwargs['data_id'] )
            if request.method == 'POST':
                #Modificacion inicial de programa
                programa = ProgramaAuditoria.objects.get(id = self.kwargs['data_id'])
                
                antigua_fecha_programa = programa.fecha_modificacion

                procesos_auditoria = int(request.POST.get("n_procesos"))
                antiguos_procesos_auditoria = int(request.POST.get("n_procesos_antiguos"))
                
                antiguo_proceso_id_grupo = request.POST.getlist('antiguo_proceso_id_grupo')
                antiguo_proceso_status_grupo = request.POST.getlist('antiguo_proceso_status_grupo')

                print(antiguo_proceso_status_grupo)

                facultades_ids = request.POST.getlist("facultades_ids")
                escuelas_ids = request.POST.getlist("escuelas_ids")

                planes = PlanAuditoria.objects.filter(programa_auditoria = programa )
                
                for plan in planes :
                    bloques = BloqueProcesoPlan.objects.filter( plan_auditoria = plan)

                    for bloque in bloques :
                        if bloque.escuela != None and not (str(bloque.escuela.id) in escuelas_ids):                     
                            bloque.delete()
                        elif bloque.escuela != None and  (str(bloque.escuela.id) in escuelas_ids): 
                            escuelas_ids.remove(str(bloque.escuela.id))
                        
                        if bloque.facultad != None and not (str(bloque.facultad.id) in facultades_ids):
                            bloque.delete()

                        elif bloque.facultad != None and (str(bloque.facultad.id) in facultades_ids):
                            facultades_ids.remove(str(bloque.facultad.id))

                print ("facultades a crear", facultades_ids)    
                print ("escuelas a crear" , escuelas_ids)    

                diccionario = crearDiccionarioFacEsc(facultades_ids,escuelas_ids)
                    
            
                
                #recorrer planes sin bloques   == 0 , borramos 
                for plan in planes:
                    n_bloques = len(BloqueProcesoPlan.objects.filter( plan_auditoria = plan))
                    print (n_bloques)

                    if n_bloques == 0:
                        plan.delete()
                        
                #programa.setattr(Auditor.objects.get(id=request.POST.get("auditor_aprobado")), 'auditor_aprobado', 'id')
                programa.auditor_aprobado = Auditor.objects.get(id=request.POST.get("auditor_aprobado"))
                programa.auditor_elaborado = Auditor.objects.get(id=request.POST.get("auditor_elaborado"))
                programa.auditor_revisado = Auditor.objects.get(id=request.POST.get("auditor_revisado"))
                programa.titulo = request.POST.get("titulo")
                programa.codigo = request.POST.get("codigo")
                programa.version = request.POST.get("version")
                programa.objetivo = request.POST.get("objetivo")
                programa.alcance = request.POST.get("alcance")
                programa.criterios_auditoria = request.POST.get("criterios_auditoria")
                programa.modalidad = request.POST.get("modalidad")
                programa.lugar = request.POST.get("lugar")
                programa.recursos = request.POST.get("recursos")
                programa.control_riesgo = request.POST.get("control_riesgo")
                programa.n_revision = 1
                programa.fecha_modificacion = datetime.now()

                if request.POST.get("firma-clear_id") == "on":
                    programa.firma = None
                if (len(request.FILES) > 0):
                    programa.firma = request.FILES.get('firma')
                
                programa.save()

                
                #Gestion de proceso_programa antiguo
                for id, state, index in zip(antiguo_proceso_id_grupo, antiguo_proceso_status_grupo, range(antiguos_procesos_auditoria)):
                    print(state)
                    if state == "0":
                        proceso = ProcesoPrograma.objects.get(id = int(id))
                        proceso.delete()
                        #print("Borrando proceso antiguo")
                    if state == "1":
                        fechas = request.POST.getlist(f'antiguo_proceso_{index+1}_fechas')
                        proceso = ProcesoPrograma.objects.get(id = int(id))
                        proceso.fecha_inicio = fechas[0]
                        proceso.fecha_fin = fechas[1]
                        proceso.save()
                        #print("Modificando proceso antiguo")
                    
                for index in range(antiguos_procesos_auditoria):
                    actividades = request.POST.getlist(f'antiguo_proceso_{index+1}_procesos')
                    actividades_id = request.POST.getlist(f'antiguo_proceso_{index+1}_procesos_id')
                    actividades_status = request.POST.getlist(f'antiguo_proceso_{index+1}_procesos_status')
                    dependencias = request.POST.getlist(f'antiguo_proceso_{index+1}_dependencias')
                    dependencias_id = request.POST.getlist(f'antiguo_proceso_{index+1}_dependencia_id')
                    dependencias_status = request.POST.getlist(f'antiguo_proceso_{index+1}_dependencia_status')

                    #Gestion de actividades antiguas
                    for proceso, id, state in zip(actividades, actividades_id, actividades_status):
                        #Elimina actividad
                        if state == "0":
                            try:
                                actividad = ActividadesProcesoPrograma.objects.get(id = int(id))
                                actividad.delete()
                            except:
                                print("Actividad no encontrada")
                        if state != "0" and id == "0":
                            #Crear nueva actividad
                            actividad = ActividadesProcesoPrograma.objects.create(
                                proceso_programa = ProcesoPrograma.objects.get(id = int(antiguo_proceso_id_grupo[index])),
                                proceso = Proceso.objects.get(id=proceso)
                            )
                        if state != "0" and id != "0":
                            #Modifica actividad
                            try:
                                actividad = ActividadesProcesoPrograma.objects.get(id = int(id))
                                actividad.proceso = Proceso.objects.get(id = int(proceso))
                                actividad.save()
                            except:
                                print("Actividad o proceso no encontrado")

                    #Gestion de dependencias antiguas
                    for dependencia, id, state in zip(dependencias, dependencias_id, dependencias_status):
                        if state == "0":
                            try:
                                dependencia_proceso = DependenciasProcesoPrograma.objects.get(id = int(id))
                                dependencia_proceso.delete()
                            except:
                                print("Dependencia no encontrada")
                        if state != "0" and id == "0":
                            #Crear nueva dependencia
                            dependencia_proceso = DependenciasProcesoPrograma.objects.create(
                                proceso_programa = ProcesoPrograma.objects.get(id = int(antiguo_proceso_id_grupo[index])),
                                dependencia = Dependencia.objects.get(id=dependencia)
                            )
                        if state != "0" and id != "0":
                            #Modifica dependencia
                            try:
                                dependencia_proceso = DependenciasProcesoPrograma.objects.get(id = int(id))
                                dependencia_proceso.dependencia = Dependencia.objects.get(id = int(dependencia))
                                dependencia_proceso.save()
                            except:
                                print("Dependencia de proceso o dependencia no encontrado")


                facultades = []
                noFacultades = []

                procesosProgramaNuevos = []

                #Gestion de nuevos procesos   
            
                for index in range(procesos_auditoria):
                    procesos = request.POST.getlist(f'proceso_{index+1}_procesos')
                    fechas = request.POST.getlist(f'proceso_{index+1}_fechas')
                    dependencias = request.POST.getlist(
                        f'proceso_{index+1}_dependencias')

                    procesoPrograma = ProcesoPrograma.objects.create(
                        programa_auditoria = programa,
                        fecha_inicio = fechas[0] ,
                        fecha_fin = fechas[1]
                    )

                    procesosProgramaNuevos += [procesoPrograma]

                    for proceso in procesos:
                        procesoAuditoria = ActividadesProcesoPrograma.objects.create(
                            proceso_programa = procesoPrograma,
                            proceso = Proceso.objects.get(id=proceso)
                        )
                    for dependencia in dependencias:
                        dependenciaTemporal = Dependencia.objects.get(id=dependencia)
                        dependenciaAuditoria = DependenciasProcesoPrograma.objects.create(
                            proceso_programa = procesoPrograma,
                            dependencia = Dependencia.objects.get(id=dependencia)
                        )
                        if dependenciaTemporal.facultad:
                            facultades += [[procesoPrograma, dependenciaTemporal]]
                        else:
                            noFacultades += [procesoPrograma]
                
                
                #ProcesoPrograma actuales del programa
                procesosProgramaActuales = list(ProcesoPrograma.objects.filter(programa_auditoria = programa))
                
                #Calculo de fecha minima y maxima
                min, max = calcularFechasMinMax(programa)
                print("Min: ", min)

                print("procesosProgramaNuevos: ", procesosProgramaNuevos)
                planes = list(PlanAuditoria.objects.filter(programa_auditoria = programa))
                for plan in planes:
                    #Actualizacion de fecha limite
                    plan.fecha_inicio = min
                    plan.fecha_fin = max
                    plan.save()
            
                    print ( " plan antiguo ", plan )
                    for proceso_actual in procesosProgramaNuevos:
                        dependenciaTemporal = DependenciasProcesoPrograma.objects.get(proceso_programa=proceso_actual)
                        dependencia = Dependencia.objects.get(id=dependenciaTemporal.dependencia.id)
                        print ("DEPENDCENCIA CREADA : " , dependencia)
                        if dependencia.facultad == True and dependencia.escuela == False:
                            try :
                                bloque_facultad = BloqueProcesoPlan.objects.get(plan_auditoria = plan , escuela__isnull= True, facultad__isnull= False) 
                            except: 
                                bloque_facultad = None
                            if bloque_facultad != None: 
                                procesos_create = ProcesoPlanAuditoria.objects.create (
                                    bloque_proceso_auditoria = bloque_facultad,
                                    proceso_programa = proceso_actual,
                                    fecha = min
                                )
                        elif dependencia.escuela == True:
                            try :
                                bloque_escuela = BloqueProcesoPlan.objects.get(plan_auditoria = plan , facultad__isnull= True) 
                            except: 
                                bloque_escuela = None
                                
                            if bloque_escuela != None: 
                                procesos_create = ProcesoPlanAuditoria.objects.create (
                                    bloque_proceso_auditoria = bloque_escuela,
                                    proceso_programa = proceso_actual,
                                    fecha = min
                                )
                        
                            
                        else :
                            try :
                                bloque_administrativo = BloqueProcesoPlan.objects.get(plan_auditoria = plan , facultad__isnull= True , escuela__isnull= True) 
                            except: 
                                bloque_administrativo = None
                            if bloque_administrativo != None: 
                                procesos_create = ProcesoPlanAuditoria.objects.create (
                                    bloque_proceso_auditoria = bloque_administrativo,
                                    proceso_programa = proceso_actual,
                                    fecha = min
                                )
            
                print("procesosProgramaActuales: ", procesosProgramaActuales)
                print("diccionario: ", diccionario)

                procesosProgramaActuales_completo = []
                for proceso_actual in procesosProgramaActuales:
                    dependenciaTemporal = DependenciasProcesoPrograma.objects.get(proceso_programa=proceso_actual)
                    dependencia = Dependencia.objects.get(id=dependenciaTemporal.dependencia.id)
                    if dependenciaTemporal != None and dependencia.facultad: 
                        procesosProgramaActuales_completo += [[proceso_actual, dependencia]]

                #Plan dependencias administrativas totalmente nuevo
                
                plan_general = False
                procesos_administrativos = False

                for proceso_actual in procesosProgramaActuales:
                    dependenciaTemporal = DependenciasProcesoPrograma.objects.get(proceso_programa=proceso_actual)
                    dependencia = Dependencia.objects.get(id=dependenciaTemporal.dependencia.id)
                    if not dependencia.escuela and not dependencia.facultad:
                        procesos_administrativos = True

                for plan in planes:
                    bloques = list(BloqueProcesoPlan.objects.filter(plan_auditoria = plan))
                    for bloque in bloques:
                        if bloque.escuela == None and bloque.facultad == None:
                            plan_general = True
                
                if not plan_general and procesos_administrativos:
                    planAuditoria = PlanAuditoria.objects.create(
                        programa_auditoria = programa,
                        titulo = "Plan de Auditoría - Dependencias Administrativas",
                        auditor_lider = programa.auditor_elaborado,
                        fecha_inicio = min,
                        fecha_fin = max
                    )

                    bloque_plan = BloqueProcesoPlan.objects.create(
                            plan_auditoria = planAuditoria,
                            titulo = "Dependencias Administrativas"
                        )
                    
                    for proceso_actual in procesosProgramaActuales:
                        #print("proceso_actual: ", proceso_actual)
                        dependenciaTemporal = DependenciasProcesoPrograma.objects.get(proceso_programa=proceso_actual)
                        dependencia = Dependencia.objects.get(id=dependenciaTemporal.dependencia.id)
                        if not dependencia.escuela and not dependencia.facultad:
                            proceso_plan = ProcesoPlanAuditoria.objects.create(
                                bloque_proceso_auditoria = bloque_plan,
                                proceso_programa = proceso_actual,
                                fecha = min
                            )

                #Planes totalmente nuevos para facultades y escuelas
                for clave, facultad in diccionario.items():
                    #Crear plan de auditoria para una facultad
                    planAuditoria = PlanAuditoria.objects.create(
                        programa_auditoria = programa,
                        titulo = "Plan de Auditoría - " + clave,
                        auditor_lider = programa.auditor_elaborado,
                        fecha_inicio = min,
                        fecha_fin = max
                    )
                    if facultad["facultad"]: 
                        #BloqueProcesoPlan
                        bloque_facultad = BloqueProcesoPlan.objects.create(
                            plan_auditoria = planAuditoria,
                            facultad = Facultad.objects.get(nombre=clave),
                            titulo = clave
                        )
                        #Procesos Plan Auditoria de Facultad
                        for procesos in procesosProgramaActuales_completo:
                            if procesos[1].escuela == False:
                                proceso_plan = ProcesoPlanAuditoria.objects.create(
                                    bloque_proceso_auditoria = bloque_facultad,
                                    proceso_programa = procesos[0],
                                    fecha = min
                                )

                    #Crear bloques y procesos plan para cada escuela de ese plan
                    for escuela in facultad["escuelas"]:
                        #BloqueProcesoPlan
                        bloque_escuela= BloqueProcesoPlan.objects.create(
                            plan_auditoria = planAuditoria,
                            escuela = Escuela.objects.get(nombre=escuela),
                            titulo = escuela,
                        )
                        for procesos in procesosProgramaActuales_completo:
                            if procesos[1].escuela == True : 
                                proceso_plan = ProcesoPlanAuditoria.objects.create(
                                    bloque_proceso_auditoria = bloque_escuela,
                                    proceso_programa = procesos[0],
                                    fecha = min
                                )
                
            
                #Programa
                #Planes
                #Bloques por plan
                #Procesos por bloque
                try:
                    rutaDocx = "static/pdfs/programa-" + str(programa.id) + antigua_fecha_programa.strftime("%Y%m%d%H%M%S") + ".docx"
                    if os.path.exists(rutaDocx):
                        remove(rutaDocx)

                    crearPdfPrograma(programa.id)
                    programa.docx_generado = True
                    programa.save()

                except:
                    print("Error al crear docx")
                    programa.docx_generado = False
                    programa.save()
                #generarPlanesPdfs(programa.id)
            return redirect("principal")
        except:
            return redirect("principal")

class SavePrograma(View):
    def get(self, request):
        if not request.user.is_authenticated:
            return  redirect("login")   
        if request.user.is_authenticated and Usuario.objects.get(user=request.user).rol != "administrador":
            return  redirect("principal")

        #form = ProgramaForm()
        procesos = Proceso.objects.all()
        dependencias = Dependencia.objects.all()
        auditores = Auditor.objects.filter(estado = True)
        facultades = Facultad.objects.all()
        escuelas = Escuela.objects.all()

        context = {
            'procesos': procesos,
            'dependencias': dependencias,
            'auditores': auditores,
            'facultades': facultades,
            'escuelas': escuelas
        }

        if request.user.is_authenticated and Usuario.objects.get(user=request.user).rol == "administrador":
            usuario = Usuario.objects.get(user=request.user)
            context["user"] = usuario
            return render(request, "programa-add.html", context)
        
        return redirect(ListPrograma)

        

    def post(self, request):
        if not request.user.is_authenticated:
            return  redirect("login")   
        if request.user.is_authenticated and Usuario.objects.get(user=request.user).rol != "administrador":
            return  redirect("principal")

        #form = ProgramaForm(request.POST, request.FILES)
        #if form.is_valid():
        if request.method == 'POST':

            auditor_lider_defecto = Auditor.objects.get(id=request.POST.get("auditor_elaborado"))

            programa = ProgramaAuditoria.objects.create(
                auditor_aprobado = Auditor.objects.get(id=request.POST.get("auditor_aprobado")),
                auditor_elaborado = Auditor.objects.get(id=request.POST.get("auditor_elaborado")),
                auditor_revisado = Auditor.objects.get(id=request.POST.get("auditor_revisado")),
                codigo = request.POST.get("codigo"),
                version = request.POST.get("version"),
                titulo = request.POST.get("titulo"),
                objetivo = request.POST.get("objetivo"),
                alcance = request.POST.get("alcance"),
                criterios_auditoria = request.POST.get("criterios_auditoria"),
                modalidad = request.POST.get("modalidad"),
                lugar = request.POST.get("lugar"),
                recursos = request.POST.get("recursos"),
                control_riesgo = request.POST.get("control_riesgo"),
                n_revision = request.POST.get("n_revision"),
                firma = request.FILES.get('firma') if (len(request.FILES) > 0) else None
            )
            
            procesos_auditoria = int(request.POST.get("n_procesos"))

            facultades_ids = request.POST.getlist("facultades_ids")
            escuelas_ids = request.POST.getlist("escuelas_ids")
            print("Facultades Ids: ", facultades_ids)
            print("Escuelas Ids: ", escuelas_ids)

            diccionario = crearDiccionarioFacEsc(facultades_ids,escuelas_ids)

            print("Diccionario: ", diccionario)
            facultades = []
            noFacultades = []

            for index in range(procesos_auditoria):
                procesos = request.POST.getlist(f'proceso_{index+1}_procesos')
                fechas = request.POST.getlist(f'proceso_{index+1}_fechas')
                dependencias = request.POST.getlist(
                    f'proceso_{index+1}_dependencias')

                procesoPrograma = ProcesoPrograma.objects.create(
                    programa_auditoria = programa,
                    fecha_inicio = fechas[0] ,
                    fecha_fin = fechas[1]
                )

                for proceso in procesos:
                    procesoAuditoria = ActividadesProcesoPrograma.objects.create(
                        proceso_programa = procesoPrograma,
                        proceso = Proceso.objects.get(id=proceso)
                    )
                for dependencia in dependencias:
                    dependenciaTemporal = Dependencia.objects.get(id=dependencia)
                    dependenciaAuditoria = DependenciasProcesoPrograma.objects.create(
                        proceso_programa = procesoPrograma,
                        dependencia = dependenciaTemporal
                    )
                    if dependenciaTemporal.facultad:
                        facultades += [[procesoPrograma, dependenciaTemporal]]
                    else:
                        noFacultades += [procesoPrograma]

            print("Facultades", facultades)
            print("No facultades", noFacultades)
           
            #Calculo de fecha minima y maxima
            min, max = calcularFechasMinMax(programa)

            #diccionario[facultad_temporal.nombre] = {"facultad": True, "escuelas": []}

            for clave, facultad in diccionario.items():
                #Crear plan de auditoria para una facultad
                verificar = False

                for procesos in facultades:
                    if procesos[1].escuela == False :
                        verificar = True
                if verificar:
                    planAuditoria = PlanAuditoria.objects.create(
                        programa_auditoria = programa,
                        titulo = "Plan de Auditoría - " + clave,
                        auditor_lider = auditor_lider_defecto,
                        fecha_inicio = min,
                        fecha_fin = max
                    )
                    if facultad["facultad"]: 
                        #BloqueProcesoPlan
                        bloque_facultad = BloqueProcesoPlan.objects.create(
                            plan_auditoria = planAuditoria,
                            facultad = Facultad.objects.get(nombre=clave),
                            titulo = clave
                        )
                        #Procesos Plan Auditoria de Facultad
                        for procesos in facultades:
                            if procesos[1].escuela == False:
                                proceso_plan = ProcesoPlanAuditoria.objects.create(
                                    bloque_proceso_auditoria = bloque_facultad,
                                    proceso_programa = procesos[0],
                                    fecha = min
                                )

                planesCreados = list(PlanAuditoria.objects.filter(programa_auditoria = programa))
                for index, planCreado in zip(range(len(planesCreados)), planesCreados):
                    planesCreados[index] = planCreado.titulo

                titulo_temporal = "Plan de Auditoría - " + clave
                if not(titulo_temporal in planesCreados):
                    planAuditoria = PlanAuditoria.objects.create(
                        programa_auditoria = programa,
                        titulo = "Plan de Auditoría - " + clave,
                        auditor_lider = auditor_lider_defecto,
                        fecha_inicio = min,
                        fecha_fin = max
                    )

                #Crear bloques y procesos plan para cada escuela de ese plan
                for escuela in facultad["escuelas"]:
                    #BloqueProcesoPlan
                    verificar = False

                    for procesos in facultades:
                        if procesos[1].escuela == True :
                            verificar = True
                       
                    if verificar:

                        bloque_escuela= BloqueProcesoPlan.objects.create(
                            plan_auditoria = planAuditoria,
                            escuela = Escuela.objects.get(nombre=escuela),
                            titulo = escuela
                        )
                        for procesos in facultades:
                            if procesos[1].escuela == True : 
                                proceso_plan = ProcesoPlanAuditoria.objects.create(
                                    bloque_proceso_auditoria = bloque_escuela,
                                    proceso_programa = procesos[0],
                                    fecha = min
                                )

            #Creacion de plan de auditoria para no facultades
            if len(noFacultades) > 0 :
                planAuditoria = PlanAuditoria.objects.create(
                    programa_auditoria = programa,
                    titulo = "Plan de Auditoría - Dependencias Administrativas",
                    auditor_lider = auditor_lider_defecto,
                    fecha_inicio = min,
                    fecha_fin = max
                )
                #BloqueProcesoPlan
                bloque_plan = BloqueProcesoPlan.objects.create(
                    plan_auditoria = planAuditoria,
                    titulo = "Dependencias Administrativas"
                )
                for procesos in noFacultades:
                    proceso_plan = ProcesoPlanAuditoria.objects.create(
                        bloque_proceso_auditoria = bloque_plan,
                        proceso_programa = procesos,
                        fecha = min
                    )
            
            # form.save()
            
            try:
                programa.docx_generado = True
                programa.save()
                crearPdfPrograma(programa.id)
            except:
                programa.docx_generado = False
                programa.save()
                print("Error al crear pdf de programa")

            #generarPlanesPdfs(programa.id)
            return redirect("principal")
        
        return redirect("principal")

def crearDiccionarioFacEsc(facultades_ids, escuelas_ids):
    diccionario = {}
    #For facultades y agregarlos al diccionario
    for facultad in facultades_ids:
        facultad_temporal = Facultad.objects.get(id=facultad)
        diccionario[facultad_temporal.nombre] = {"facultad": True, "escuelas": []}

    #For escuelas - verificar si el facultad_id existe en facultades
    for escuela in escuelas_ids:
        escuela_temporal = Escuela.objects.get(id=escuela)
        facultad_escuela = Facultad.objects.get(id = escuela_temporal.facultad_id)
        if facultad_escuela.nombre in diccionario.keys():
            diccionario[facultad_escuela.nombre]["escuelas"] += [escuela_temporal.nombre]
        else:
            diccionario[facultad_escuela.nombre] = {"facultad": False, "escuelas": [escuela_temporal.nombre]}

    return diccionario

def calcularFechasMinMax(programa):
    FechasInicio = []
    FechasFin = []
    procesos_programa = ProcesoPrograma.objects.filter(programa_auditoria=programa)

    for procesos_unicos in procesos_programa:
        FechasInicio += [procesos_unicos.fecha_inicio]
        FechasFin += [procesos_unicos.fecha_fin]

    menorFecha = min(FechasInicio)
    mayorFecha = max(FechasFin)

    return menorFecha, mayorFecha