let dataJsonAuditores = [];

let url = [{ listar: "/user/listarAuditores" }];

var form = document.getElementById("formID");
var submitButton = document.getElementById("submitID");

submitButton.addEventListener(
  "click",
  function () {
    inputs = document.getElementsByTagName("input");
    selects = document.getElementsByTagName("select");

    var horas = [];
    var auditoresIds = [];
    var auditoresValues = [];
    var fechasIds = [];

    //Rellenar horas
    for (var i = 0; i < inputs.length; i++) {
      if (inputs[i].id.includes("hora")) {
        document.getElementById(inputs[i].id).setCustomValidity("");
        horas.push(inputs[i].id);
      }
    }
    //Comprobar horas
    for (var i = 0; i < horas.length; i += 2) {
      TTime(horas[i], horas[i + 1]);
    }

    //Rellenar auditores
    for (var i = 0; i < selects.length; i++) {
      if (
        selects[i].id.includes("select_auditor") &
        (selects[i].parentElement.parentElement.style.display != "none")
      ) {
        document.getElementById(selects[i].id).setCustomValidity("");
        auditoresIds.push(selects[i].id);
        auditoresValues.push(selects[i].value);
      }
    }
    //Comprobar auditores
    TAuditores(auditoresIds, auditoresValues);

    //Rellenar fechas
    for (var i = 0; i < inputs.length; i++) {
      if (inputs[i].name.includes("proceso_plan_fechas")) {
        document.getElementById(inputs[i].id).setCustomValidity("");
        fechasIds.push(inputs[i].id);
      }
    }
    //Comprobar fecha
    TFechas(fechasIds);
  },
  false
);

function TFechas(fechasIds) {
  fecha_inicio = document.getElementById("fecha_inicio");
  fecha_fin = document.getElementById("fecha_fin");

  for (var i = 0; i < fechasIds.length; i++) {
    fecha_actual = document.getElementById(fechasIds[i]);
    if (fecha_actual.value < fecha_inicio.value || fecha_actual.value > fecha_fin.value){
      fecha_actual.setCustomValidity("Fecha fuera de rango");
    }
  }
}

function TAuditores(auditoresIds, auditoresValues) {
  var comprobados = [];

  for (var i = 0; i < auditoresIds.length; i++) {
    if (comprobados.includes(auditoresValues[i])) {
      elemento = document.getElementById(auditoresIds[i]);
      elemento.setCustomValidity("No se puede repetir auditores");
    } else {
      comprobados.push(auditoresValues[i]);
    }
  }
}

function TTime(id1, id2) {
  var UserDate = document.getElementById(id1).value;
  var ToDate = document.getElementById(id2).value;
  var time = ToDate.split(":");
  console.log(time);
  if (UserDate > ToDate) {
    var a = document.getElementById(id2);
    a.setCustomValidity("La hora inicial debe ser menor a la de fin");
    return false;
  }
  return true;
}

function validateRevision(e) {
  var numbers = /^[0-9]+$/;
  var inputtxt = document.getElementById("n_revision");
  if (!inputtxt.value.match(numbers)) {
    newstring = inputtxt.value.replace(/\D+/g, "");
    inputtxt.value = newstring;
  }
}

form.addEventListener(
  "submit",
  function () {
    // Disable the submit button
    submitButton.setAttribute("disabled", "disabled");

    // Change the "Submit" text
    submitButton.value = "Please wait...";
  },
  false
);

function antiguoChangeCheckbox(id) {
  var elementoRemover = document.getElementById(id);
  var elementoInputStatus = document.getElementById(id).previousElementSibling;
  var elementoInputId =
    document.getElementById(id).previousElementSibling.previousElementSibling;

  if (elementoRemover.checked) {
    elementoInputStatus.value = "1";
  } else {
    elementoInputStatus.value = "0";
  }
}

function changeCheckbox(id) {
  var elementoSeleccionado = document.getElementById(id);
  var elementoDesabilitado = elementoSeleccionado.previousElementSibling;

  if (elementoSeleccionado.checked) {
    elementoDesabilitado.removeAttribute("disabled");
  } else {
    elementoDesabilitado.setAttribute("disabled", "");
  }
}

const listarData = async () => {
  try {
    const responseAuditores = await fetch(url[0].listar);
    const dataAuditores = await responseAuditores.json();

    //Asignar procesos
    if (dataAuditores.message === "Success") {
      console.log(dataAuditores);
      dataJsonAuditores = dataAuditores;
    } else {
      alert("Auditores no encontrados...");
    }
  } catch (error) {
    console.log(error);
  }
};

const cargaInicial = async () => {
  await listarData();
};

window.addEventListener("load", async () => {
  await cargaInicial();
  inicialesLoad();
});

function CrearFunciones() {
  jQuery("#Funciones").append(
    '<tr><td><input style="width: 100%;" name="funciones" required type="text" class="form-control" id="" placeholder="Funcion"></td></tr>'
  );
}
function AñadirAuditores() {
  jQuery("#Auditores").append("<tr><td>" + strSelectAuditores() + "</td></tr>");

  equipos_checks = document.getElementsByClassName("td7");
  for (var i = 0; i < equipos_checks.length; i++) {
    numero_checks = equipos_checks.item(i).getElementsByTagName("div");
    equipos_checks.item(i).innerHTML +=
      '<div class="col-md-4"><input disabled type="number" id="auditor_checkbox_position_' +
      (i + 1) +
      "_" +
      (numero_checks.length + 1) +
      '" name="auditor_checkbox_position_' +
      (i + 1) +
      '" value="' +
      (numero_checks.length + 1) +
      '" style="display: none; margin-bottom: 10px;" /><input class="form-check-input nuevo" type="checkbox"' +
      "onchange=\"changeCheckbox('auditor_checkbox_" +
      (i + 1) +
      "_" +
      (numero_checks.length + 1) +
      "')\"" +
      'id="auditor_checkbox_' +
      (i + 1) +
      "_" +
      (numero_checks.length + 1) +
      '" value="' +
      Auditores.lastElementChild.firstElementChild.firstElementChild.value +
      '" name="auditor_checkbox_' +
      (i + 1) +
      '"/><label class="form-check-label" for="inlineCheckbox1">' +
      iniciales(
        Auditores.lastElementChild.firstElementChild.firstElementChild
          .firstElementChild.text
      ) +
      "</label></div>";
  }

  inputs = document.getElementsByTagName("input");
  antiguosCheckbox = [];

  for (var i = 0; i < inputs.length; i++) {
    if (inputs[i].id.includes("antiguo_auditor_checkbox_status_")) {
      antiguosCheckbox.push(inputs[i]);
    }
  }

  for (var i = 0; i < antiguosCheckbox.length; i++) {
    if (antiguosCheckbox[i].parentElement.style.display == "none") {
      antiguosCheckbox[i].value = "0";
    }
  }
}

function inicialesLoad() {
  labels = document.getElementsByClassName("antiguo-initials-name");

  for (var i = 0; i < labels.length; i++) {
    labels[i].innerHTML = iniciales(labels[i].innerHTML);
  }
}

function iniciales(nombre) {
  separador = " "; // un espacio en blanco
  arregloDeSubCadenas = nombre.split(separador); // SEPARA EL NOMBRE EN CADENAS INDIVIDUALES
  cadenaNueva = "";

  // IMPRIME LA PRIMERA LETRA DE CADA CADENA
  for (x = 0; x < arregloDeSubCadenas.length; x++) {
    cadenaNueva += arregloDeSubCadenas[x].substring(0, 1);
  }
  return cadenaNueva;
}

function cambioAuditor(id, num) {
  labels = [];
  checks = [];

  select = document.getElementById(id);
  select_id = select.value;
  select_nombre = select.options[select.selectedIndex].text;

  contenedores = document.getElementsByClassName("td7");

  for (var i = 0; i < contenedores.length; i++) {
    labels = labels.concat([
      contenedores[i].getElementsByTagName("label")[num],
    ]);

    contenedores[i].getElementsByTagName("label")[num].innerHTML =
      iniciales(select_nombre);

    //document.getElementsByClassName("td7")[0].getElementsByClassName("col-md-4")[2].firstElementChild
    if (
      !contenedores[i]
        .getElementsByClassName("col-md-4")
        [num].id.includes("antiguo")
    ) {
      contenedores[i].getElementsByClassName("col-md-4")[
        num
      ].firstElementChild.nextElementSibling.value = select_id;
    }
    //contenedores[i].getElementsByTagName("input")[num].value = select_id;

    checks = checks.concat([
      contenedores[i].getElementsByTagName("input")[num],
    ]);
  }

  console.log(labels);
  console.log(checks);
}

function strSelectAuditores() {
  num = document.getElementsByClassName("select_auditor").length;

  stringAuditores =
    '<select class="select_auditor form-control" name="auditores" onchange="cambioAuditor(' +
    "'select_auditor_" +
    (num + 1) +
    "'" +
    "," +
    num +
    ')" id="select_auditor_' +
    (num + 1) +
    '">';

  for (var i = 0; i < dataJsonAuditores.objetos.length; i++) {
    stringAuditores +=
      '<option value="' +
      dataJsonAuditores.objetos[i].id +
      '">' +
      dataJsonAuditores.usuarios[i].nombre +
      "</option>";
  }

  stringAuditores += "</select>";

  return stringAuditores;
}

function AñadirTabla() {
  jQuery("#tableBOT").append(
    '<tr id = "proceso_1"><td id="proceso_1_td1"><fieldset class="form-group date-icon"><input type="date"class="col-md-10"style="margin-right: 10%;margin-top: 6%;margin-left: 2%;"id="fecha1_1"name="proceso_1_fechas"/><br /><br /></fieldset></td><td id="proceso_1_td2"><fieldset class="form-group date-icon"><input type="time"class="col-md-5"style="margin-right: 10%;margin-top: 6%;"id="fecha1_1"name="proceso_1_fechas"/><input type="time" class="col-md-5" style="margin-top: 6%;" id="fecha1_2" name="proceso_1_fechas"/><br /><br /></fieldset></td><td id="proceso_1_td3"><input style="width: 90%;" type="textarea" class="form-control" id="" placeholder="Lugar"></td><td id="proceso_1_td3"><input style="width: 90%;" type="textarea" class="form-control" id="" placeholder="Actividad"></td><td id="proceso_1_td3"><input style="width: 90%;" type="textarea" class="form-control" id="" placeholder="Proceso"></td><td id="proceso_1_td3"><div class="col-md-4"><input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1"> <label class="form-check-label" for="inlineCheckbox1">RD</label></div><div class="col-md-4"><input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="option2"> <label class="form-check-label" for="inlineCheckbox2">DF</label></div><div class="col-md-4"><input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="option2"> <label class="form-check-label" for="inlineCheckbox2">AR</label></div><div class="col-md-4"><input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="option2"> <label class="form-check-label" for="inlineCheckbox2">LA</label></div></td></tr>'
  );
}
function removerAuditores() {
  //tr
  var elementoRemover = Auditores.lastElementChild;
  iterator = 0;

  //Seleccion de elemento
  while (
    elementoRemover != null &&
    elementoRemover.id.includes("antiguo_auditor_tr") &&
    elementoRemover.style.display == "none"
  ) {
    elementoRemover = elementoRemover.previousElementSibling;
    iterator++;
  }

  if (elementoRemover == null) {
    return;
  }

  //Ocultacion
  if (elementoRemover.id.includes("antiguo_auditor_tr")) {
    //Ocultar y marcar auditor
    elementoRemover.style.display = "none";
    elementoRemover.firstElementChild.firstElementChild.nextElementSibling.value =
      "0";
    //Marcar checkboxes
    equipos_checks = document.getElementsByClassName("td7");
    for (var i = 0; i < equipos_checks.length; i++) {
      proceso_equipo_checks = equipos_checks
        .item(i)
        .getElementsByTagName("div");
      proceso_equipo_checks[
        proceso_equipo_checks.length - iterator - 1
      ].style.display = "none";
      input_status =
        proceso_equipo_checks[
          proceso_equipo_checks.length - iterator - 1
        ].getElementsByTagName("input");
      if (input_status.length >= 2) {
        input_status[1].value = "0";
      }
      console.log(
        proceso_equipo_checks[proceso_equipo_checks.length - iterator - 1]
      );
    }
  }
  //Eliminacion
  else {
    elementoRemover.remove();
    equipos_checks = document.getElementsByClassName("td7");

    for (var i = 0; i < equipos_checks.length; i++) {
      equipos_checks.item(i).lastElementChild.remove();
    }
  }
}
function removerFunciones() {
  //tr
  var elementoRemover = Funciones.lastElementChild;
  iterator = 0;

  //Seleccion de elemento
  while (
    elementoRemover != null &&
    elementoRemover.id.includes("antiguo_funcion_tr") &&
    elementoRemover.style.display == "none"
  ) {
    elementoRemover = elementoRemover.previousElementSibling;
    iterator++;
  }

  if (elementoRemover == null) {
    return;
  }

  //Ocultacion
  if (elementoRemover.id.includes("antiguo_funcion_tr")) {
    //Ocultar y marcar auditor
    elementoRemover.style.display = "none";
    elementoRemover.firstElementChild.firstElementChild.nextElementSibling.value =
      "0";
  }
  //Eliminacion
  else {
    elementoRemover.remove();
  }
}

function removerTabla() {
  tableBOT.lastElementChild.remove();
}
