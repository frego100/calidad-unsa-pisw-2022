let contadores = [];
let contadoresAntiguos = [];

let referenciaProcesos = [];

let dataJsonProcesos = [];
let dataJsonDependencias = [];

let url = [
  { listar: "/administracion/listarProcesos" },
  { listar: "/administracion/listarDependencias" },
];

var form = document.getElementById("formID");
var submitButton = document.getElementById("submitID");

submitButton.addEventListener(
  "click",
  function () {
    inputs = document.getElementsByTagName("input");
    selects = document.getElementsByTagName("select");
    var fechas = [];
    var dependenciasIds = [];
    var dependenciasValues = [];

    //Rellenar fechas
    for (var i = 0; i < inputs.length; i++) {
      if (inputs[i].id.includes("fecha")) {
        document.getElementById(inputs[i].id).setCustomValidity("");
        fechas.push(inputs[i].id);
      }
    }
    //Comprobar fechas
    for (var i = 0; i < fechas.length; i += 2) {
      TDate(fechas[i], fechas[i + 1]);
    }

    //Rellenar dependencias
    for (var i = 0; i < selects.length; i++) {
      if (selects[i].id.includes("dependencia") & selects[i].parentElement.parentElement.style.display != "none") {
        document.getElementById(selects[i].id).setCustomValidity("");
        dependenciasIds.push(selects[i].id);
        dependenciasValues.push(selects[i].value);
      }
    }
    //Comprobar dependencias
    TDependencia(dependenciasIds, dependenciasValues);
  },
  false
);

function validateRevision(e) {
  var numbers = /^[0-9]+$/;
  var inputtxt = document.getElementById("n_revision");
  if (!inputtxt.value.match(numbers)) {
    newstring = inputtxt.value.replace(/\D+/g, "");
    inputtxt.value = newstring;
  }
}

function TDependencia(dependenciasIds, dependenciasValues) {
  var comprobados = [];

  for (var i = 0; i < dependenciasIds.length; i++) {
    if (comprobados.includes(dependenciasValues[i])) {
      elemento = document.getElementById(dependenciasIds[i]);
      elemento.setCustomValidity(
        "No se puede repetir elementos para dependencias"
      );
    } else {
      comprobados.push(dependenciasValues[i]);
    }
  }
}

function TDate(id1, id2) {
  var UserDate = document.getElementById(id1).value;
  var ToDate = document.getElementById(id2).value;

  if (new Date(UserDate).getTime() > new Date(ToDate).getTime()) {
    //alert("The Date must be Bigger or Equal to today date");
    var a = document.getElementById(id2);
    //a.oninvalid="this.setCustomValidity('Please Enter valid email')"
    a.setCustomValidity("La fecha inicial debe ser menor a la de fin");
    return false;
  }
  return true;
}

form.addEventListener(
  "submit",
  function () {
    // Disable the submit button
    submitButton.setAttribute("disabled", "disabled");

    // Change the "Submit" text
    submitButton.value = "Please wait...";
  },
  false
);

function contarProcesosDependencias() {
  for (var i = 1; i <= n_procesos_antiguos.value; i++) {
    elements = document.getElementsByName(
      "antiguo_proceso_" + i + "_procesos"
    ).length;
    console.log("Procesos: " + elements);
  }
}

const peticion = async () => {
  try {
    const responseProcesos = await fetch("/programa/peticion");
  } catch (error) {
    console.log(error);
  }
};

const csrftoken = document.querySelector("[name=csrfmiddlewaretoken]").value;
var eliminados = { actividades: [1, 2, 3], dependencias: [4, 5, 6] };

function peticion2() {
  $.ajax({
    headers: { "X-CSRFToken": csrftoken },
    type: "POST",
    url: "/programa/peticion2",
    data: {
      result: JSON.stringify(eliminados),
    },
    dataType: "json",
    success: function (data) {
      // any process in data
      alert("successfull");
    },
    failure: function () {
      alert("failure");
    },
  });
}

function removeActividadAntiguo(id, id_proceso) {
  var elementoRemover = document.getElementById(id).lastElementChild;
  var elementoInputStatus =
    document.getElementById(id).lastElementChild.previousElementSibling;
  var elementoInputId =
    document.getElementById(id).lastElementChild.previousElementSibling
      .previousElementSibling;

  while (
    elementoRemover != null &&
    elementoRemover.tagName == "SELECT" &&
    elementoRemover.style.display == "none"
  ) {
    elementoRemover =
      elementoRemover.previousElementSibling.previousElementSibling
        .previousElementSibling;
    elementoInputStatus =
      elementoInputStatus.previousElementSibling.previousElementSibling
        .previousElementSibling;
    elementoInputId =
      elementoInputId.previousElementSibling.previousElementSibling
        .previousElementSibling;
  }

  if (elementoInputId == null) {
    return;
  }
  if (elementoInputId.value == "0") {
    elementoInputId.remove();
    elementoInputStatus.remove();
    elementoRemover.remove();
  } else if (elementoInputId.value != "0") {
    elementoInputId.style.display = "none";
    elementoInputStatus.style.display = "none";
    elementoInputStatus.value = "0";
    elementoRemover.style.display = "none";
  }
}

function removeDependenciaAntiguo(id, id_proceso) {
  removeActividadAntiguo(id, id_proceso);
}

function removeActividad(id, id_proceso) {
  var elementoRemover = document.getElementById(id).lastElementChild;
  if (elementoRemover.id != "") {
    elementoRemover.remove();
    contadores[id_proceso][0]--;
  }
}

function removeDependencia(id, id_proceso) {
  var elementoRemover = document.getElementById(id).lastElementChild;
  if (elementoRemover.id != "") {
    elementoRemover.remove();
    contadores[id_proceso][1]--;
  }
}

const listarData = async () => {
  try {
    const responseProcesos = await fetch(url[0].listar);
    const responseDependencias = await fetch(url[1].listar);
    const dataProcesos = await responseProcesos.json();
    const dataDependencias = await responseDependencias.json();

    //Asignar procesos
    if (dataProcesos.message === "Success") {
      console.log(dataProcesos.objetos);
      dataJsonProcesos = dataProcesos.objetos;
    } else {
      alert("Procesos no encontrados...");
    }

    //Asignar dependencias
    if (dataDependencias.message === "Success") {
      console.log(dataDependencias.objetos);
      dataJsonDependencias = dataDependencias.objetos;
    } else {
      alert("Dependencias no encontrados...");
    }
  } catch (error) {
    console.log(error);
  }
};

const cargaInicial = async () => {
  await listarData();
  contarProcesosDependencias();
};

window.addEventListener("load", async () => {
  await cargaInicial();
});

function extraSelectAntiguo(id, num) {
  jQuery("#" + id).append(strSelectProcesosAntiguo(id, num));
}

function extraSelect2Antiguo(id, num) {
  jQuery("#" + id).append(strSelectDependenciasAntiguo(id, num));
}

function strSelectProcesosAntiguo(id, num) {
  var contador = document
    .getElementById(id)
    .getElementsByTagName("select").length;

  stringProcesos =
    '<input type="number" id="antiguo_proceso_' +
    num +
    "_id_" +
    (contador + 1) +
    '" name="antiguo_proceso_' +
    num +
    '_procesos_id" value="0" style="display: none; margin-bottom: 10px" /> <input type="number" id="antiguo_proceso_' +
    num +
    "_status_" +
    (contador + 1) +
    '" name="antiguo_proceso_' +
    num +
    '_procesos_status" value="1" style="display: none; margin-bottom: 10px"/><select class="form-control select2" id="antiguo_proceso' +
    num +
    "_" +
    (contador + 1) +
    '" name="antiguo_proceso_' +
    num +
    '_procesos">';

  for (var i = 0; i < dataJsonProcesos.length; i++) {
    stringProcesos +=
      '<option value="' +
      dataJsonProcesos[i].id +
      '">' +
      dataJsonProcesos[i].nombre +
      "</option>";
  }

  stringProcesos += "</select>";

  return stringProcesos;
}

function strSelectDependenciasAntiguo(id, num) {
  var contador = document
    .getElementById(id)
    .getElementsByTagName("select").length;

  stringDependencias =
    '<input type="number" id="antiguo_dependencia_' +
    num +
    "_id_" +
    (contador + 1) +
    '" name="antiguo_proceso_' +
    num +
    '_dependencia_id" value="0" style="display: none; margin-bottom: 10px" /> <input type="number" id="antiguo_dependencia_' +
    num +
    "_status_" +
    (contador + 1) +
    '" name="antiguo_proceso_' +
    num +
    '_dependencia_status" value="1" style="display: none; margin-bottom: 10px" /><select class="form-control select2" id="antiguo_dependencia' +
    num +
    "_" +
    (contador + 1) +
    '" name="antiguo_proceso_' +
    num +
    '_dependencias">';

  for (var i = 0; i < dataJsonDependencias.length; i++) {
    stringDependencias +=
      '<option value="' +
      dataJsonDependencias[i].id +
      '">' +
      dataJsonDependencias[i].nombre +
      "</option>";
  }

  stringDependencias += "</select>";
  return stringDependencias;
}

function extraSelect(id, num) {
  contadores[num - 1][0] = contadores[num - 1][0] + 1;
  jQuery("#" + id).append(strSelectProcesos(num));
}

function extraSelect2(id, num) {
  contadores[num - 1][1] = contadores[num - 1][1] + 1;
  jQuery("#" + id).append(strSelectDependencias(num));
}

function strSelectProcesos(num) {
  stringProcesos =
    '<select class="form-control select2" id="proceso' +
    num +
    "_" +
    contadores[num - 1][0] +
    '" name="proceso_' +
    num +
    '_procesos">';

  for (var i = 0; i < dataJsonProcesos.length; i++) {
    stringProcesos +=
      '<option value="' +
      dataJsonProcesos[i].id +
      '">' +
      dataJsonProcesos[i].nombre +
      "</option>";
  }

  stringProcesos += "</select>";

  return stringProcesos;
}

function strSelectDependencias(num) {
  stringDependencias =
    '<select class="form-control select2" id="dependencia' +
    num +
    "_" +
    contadores[num - 1][1] +
    '" name="proceso_' +
    num +
    '_dependencias">';

  for (var i = 0; i < dataJsonDependencias.length; i++) {
    stringDependencias +=
      '<option value="' +
      dataJsonDependencias[i].id +
      '">' +
      dataJsonDependencias[i].nombre +
      "</option>";
  }

  stringDependencias += "</select>";
  return stringDependencias;
}

function CreateTable() {
  addProceso();
  jQuery("#table").append(
    '<tr id="proceso_' +
      n_procesos.value +
      '"><td id="proceso_' +
      n_procesos.value +
      '_td1"><button type="button"  class="btn btn-primary btn-sm "  onclick="extraSelect(' +
      "'proceso_" +
      n_procesos.value +
      "_td1'," +
      n_procesos.value +
      ')">Añadir</button> ' +
      ' <button type="button"class="btn btn-danger btn-sm pull-right" onclick="removeActividad(' +
      "'proceso_" +
      n_procesos.value +
      "_td1'," +
      (n_procesos.value - 1) +
      ')"><i class="fa fa-plus" aria-hidden="true"></i> Eliminar</button>' +
      strSelectProcesos(n_procesos.value) +
      '</td><td id="proceso_' +
      n_procesos.value +
      '_td2"><fieldset class="form-group date-icon">' +
      strSelectFechas(n_procesos.value) +
      '<br><br></fieldset></td><td id="proceso_' +
      n_procesos.value +
      '_td3">' +
      strSelectDependencias(n_procesos.value) +
      "</td></tr>"
  );
}

function addProceso() {
  n_procesos.value = parseInt(n_procesos.value) + 1;
  contadores = contadores.concat([[1, 1]]); //Agrega un arr de contadores
  console.log("Procesos: " + n_procesos.value);
  console.log("Antiguos: " + n_procesos_antiguos.value);
  console.log(contadores);
}

function removeProceso() {
  var elementoRemover = table.lastElementChild;
  //Busca elementos antiguos sin ocultar
  while (
    elementoRemover != null &&
    elementoRemover.id.includes("antiguo_proceso_") &&
    elementoRemover.style.display == "none"
  ) {
    elementoRemover = elementoRemover.previousElementSibling;
  }

  if (elementoRemover == null) {
    return;
  }

  //Elimina(Oculta) el ultimo elemento si es antiguo
  if (elementoRemover.id.includes("antiguo_proceso_")) {
    elementoRemover.style.display = "none";
    elementoRemover.firstElementChild.firstElementChild.nextElementSibling.value =
      "0";
  } else {
    //Elimina si es nuevo
    elementoRemover.remove();
    n_procesos.value = parseInt(n_procesos.value) - 1;
    if (contadores.length == 1 || contadores.length == 0) {
      contadores = [];
    } else {
      contadores.splice(contadores.length - 1); //Quita el ultimo de los contadores
    }
  }
}

function strSelectFechas(num) {
  date = new Date();
  year = date.getFullYear();
  month = date.getMonth() + 1;
  day = date.getDate();
  if (day < 10) day = "0" + day; //agrega cero si el menor de 10
  if (month < 10) month = "0" + month; //agrega cero si el menor de 10
  return (
    '<input type="date" requiered class="col-md-5 " style="margin-right: 10%;margin-top: 6%;margin-left: 2%;" id="fecha' +
    num +
    '_1" name="proceso_' +
    num +
    '_fechas" value="' +
    year +
    "-" +
    month +
    "-" +
    day +
    '"><input type="date" required style="margin-top: 6%;" class="col-md-5 " id="fecha' +
    num +
    '_2" name="proceso_' +
    num +
    '_fechas" value="' +
    year +
    "-" +
    month +
    "-" +
    day +
    '"></input>'
  );
}
