/* Esta función recolectará la entrada del usuario
y tomando en cuenta dicha entrada mostrara u ocultará información 
de una fila en particular */

//Procesos
function setProcesoEditar(id, nombre) {
  editModal.firstElementChild.action = "editproceso/"+id;
  edit_nombre.value = nombre;
}
function setProcesoBorrar(id, nombre) {
  deleteModal.firstElementChild.action = "delete_proceso/"+id;
  delete_nombre.value = nombre;
}
//Clausulas
function setClausulaEditar(id, nombre) {
  editModal.firstElementChild.action = "editclausula/"+id;
  edit_nombre.value = nombre;
}
function setClausulaBorrar(id, nombre) {
  deleteModal.firstElementChild.action = "delete_clausula/"+id;
  delete_nombre.value = nombre;
}

//Dependencias
function setDependenciaEditar(id, nombre, facultad, escuela) {
  editModal.firstElementChild.action = "editdependencia/"+id;
  edit_nombre.value = nombre;
  if(escuela == "True"){
    edit_checkbox_escuelas.checked = true;
  }
  else if(facultad == "True"){
    edit_checkbox_facultades.checked = true;
  }
  else{
    edit_checkbox_administrativos.checked = true;
  }
  
}
function setDependenciaBorrar(id, nombre, facultad, escuela) {
  deleteModal.firstElementChild.action = "delete_dependencia/"+id;
  delete_nombre.value = nombre;
  if(escuela == "True"){
    delete_checkbox_escuelas.checked = true;
  }
  else if(facultad == "True"){
    delete_checkbox_facultades.checked = true;
  }
  else{
    delete_checkbox_administrativos.checked = true;
  }
}

//Facultades
function setFacultadEditar(id, nombre) {
  editModal.firstElementChild.action = "editfacultad/"+id;
  edit_nombre.value = nombre;
}
function setFacultadBorrar(id, nombre) {
  deleteModal.firstElementChild.action = "delete_facultad/"+id;
  delete_nombre.value = nombre;
}

//Escuelas
function setEscuelaEditar(id, nombre, facultad_id) {
  editModal.firstElementChild.action = "editescuela/"+id;
  edit_nombre.value = nombre;
  facultad_id_edit.value = facultad_id;
}
function setEscuelaBorrar(id, nombre) {
  deleteModal.firstElementChild.action = "delete_escuela/"+id;
  delete_nombre.value = nombre;
}

function filtro() {
  // Accede al texto de entrada y a varios elementos del DOM
  let valor = document.getElementById("searchInput").value.toUpperCase();
  let nombres = document.getElementById("cuerpo-tabla");
  let filas = nombres.getElementsByTagName("tr");

  // Iteramos el arreglo de filas
  for (let i = 0; i < filas.length; i++) {
    // Por cada fila se obtiene la referencia a la columna lenguaje
    let columnaLenguaje = filas[i].getElementsByTagName("td")[1];
    // Se extrae el texto de la columna lenguaje
    let lenguaje = columnaLenguaje.textContent;
    // Se muestra u oculta la fila si la entrada de texto
    // coincide con el texto de la columna lenguaje
    filas[i].style.display =
      lenguaje.toUpperCase().indexOf(valor) > -1 ? "" : "none";
  }
}

// Se agrega un escuchador de eventos a la entrada de texto
// para activar la función de filtrado cada vez que el usuario
// ingresa texto en la entrada
document.getElementById("searchInput").addEventListener("keyup", filtro);



function setUsuarioEditar(id, user, rol) {
  editModal.firstElementChild.action = "usuarios/"+id;
  edit_nombre.value = user;
  if(rol == "administrador"){
    edit_checkbox_administrativos.checked = true;
    edit_auditado.style  = ""
  }
  else if(rol == "auditor"){
    edit_checkbox_auditor.checked = true;
    edit_auditado.style = ""
  }
  else{
    edit_checkbox_normal.checked = true;
    edit_auditado.style = "display:none"
  }
  
}
function setUsuarioEditar2(id, user, rol,id2,dni,dependencia,cargo) {
  editModal.firstElementChild.action = "usuarios/"+id;
  edit_nombre.value = user;
  edit_dni.value = dni;
  edit_dependencia.value = dependencia;
  edit_cargo.value = cargo;
  edit_audi_id.value = id2;


  if(rol == "administrador"){
    edit_checkbox_administrativos.checked = true;
    edit_auditado.style  = ""
  }
  else if(rol == "auditor"){
    edit_checkbox_auditor.checked = true;
    edit_auditado.style = ""
  }
  else{
    edit_checkbox_normal.checked = true;
    edit_auditado.style = "display:none"
  }
  
}
function cambio(valor){
  if (valor ==0 ){
      edit_auditado.style = ""
  }else{
      edit_auditado.style = "display:none"
     
  }
  
}