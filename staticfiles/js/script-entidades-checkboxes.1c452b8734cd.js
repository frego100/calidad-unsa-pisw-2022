function showCheckboxes(id) {
  var checkboxes = document.getElementById(id);
  if (checkboxes.style.display == "none" || checkboxes.style.display == "") {
    checkboxes.style.display = "block";
  } else {
    checkboxes.style.display = "none";
  }
}
