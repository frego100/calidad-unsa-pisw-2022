var btn1 = document.getElementById("boton1s") === null ? boton2s : boton1s;
var listaSubBotones = [btn1, boton2s, boton3s, boton4s, boton5s, boton6s];

var elementosPrograma = [programa, programas, programa_simbolo];
var elementosPlan = [plan, plans, plan_simbolo];
var elementosInforme = [informe, informes, informe_simbolo];

const tabla = document.getElementById("cuerpo-tabla");

var elementosActa =
  document.getElementById("acta") === null
    ? elementosPrograma
    : [acta, actas, acta_simbolo];

var elementosAuditor =
  document.getElementById("auditor") === null
    ? elementosPrograma
    : [auditor, auditors, auditor_simbolo];

var elementosLista =
  document.getElementById("lista") === null
    ? elementosPrograma
    : [lista, listas, lista_simbolo];

var elementosDatos =
  document.getElementById("datos") === null
    ? elementosPrograma
    : [dato, datos, dato_simbolo];

var listaBotones = [
  elementosPrograma,
  elementosPlan,
  elementosInforme,
  elementosActa,
  elementosAuditor,
  elementosLista,
  elementosDatos,
];

function toggleSidebar() {
  document.body.classList.toggle("open");
  if(document.body.classList.toggle("barraVacia")){
    document.body.classList.toggle("barraVacia");
  }

  listaSubBotones.forEach((element) => {
    element.innerText = "";
    element.disabled = false;
  });
}

function reset() {
  listaBotones.forEach((element) => {
    element[0].style.backgroundColor = "#f6f0f0";
    element[1].style.color = "black";
    element[2].style.borderRight = "4px solid black";
    element[2].style.borderBottom = "4px solid black";
  });
}

function colores(index) {
  listaBotones[index][0].style.backgroundColor = "#C80303";
  listaBotones[index][1].style.color = "white";
  listaBotones[index][2].style.borderRight = "4px solid white";
  listaBotones[index][2].style.borderBottom = "4px solid white";
}

function barraPrograma() {
  reset();
  colores(0);
  //Cambio en el siguiente menu
  btn1.innerText = "Crear Programa";
  btn1.setAttribute("href", "/programa/add");
  boton2s.innerText = "Ver Programas";
  boton2s.setAttribute("href", "/programa/view");
  boton3s.innerText = "";
  boton4s.innerText = "";
  boton5s.innerText = "";
  segundaBarra();

  //tabla.innerHTML += `<tr><td></td><td></td><td></td><td></td><td></td><td><a href="#" class="view" title="View" data-toggle="tooltip"><i class="material-icons">&#xE417;</i></a><a href="#" class="edit" title="Edit" data-toggle="tooltip"><i class="material-icons">&#xE254;</i></a><a href="#" class="delete" title="Delete" data-toggle="tooltip"><i class="material-icons">&#xE872;</i></a><a href="#" class="pdf" title="pdf"data-toggle="tooltip"><i class="material-icons">	&#xe415;</i></a></td></tr> `;
}

function barraPlan() {
  reset();
  colores(1);

  //Cambio en el siguiente menu
  /*btn1.innerText = "Crear Plan";
  btn1.setAttribute("href", "/plan/add");
  boton2s.innerText = "Ver Planes";
  boton2s.setAttribute("href", "/plan/view");*/
  btn1.innerText = "Ver Planes";
  btn1.setAttribute("href", "/plan/view")
  boton2s.innerText = "";
  boton3s.innerText = "";
  boton4s.innerText = "";
  boton5s.innerText = "";
  boton6s.innerText = "";
  segundaBarra();
  //tabla.innerHTML += `<tr><td></td><td></td><td></td><td></td><td></td><td><a href="#" class="view" title="View" data-toggle="tooltip"><i class="material-icons">&#xE417;</i></a><a href="#" class="edit" title="Edit" data-toggle="tooltip"><i class="material-icons">&#xE254;</i></a><a href="#" class="delete" title="Delete" data-toggle="tooltip"><i class="material-icons">&#xE872;</i></a><a href="#" class="pdf" title="pdf"data-toggle="tooltip"><i class="material-icons">	&#xe415;</i></a></td></tr> `;
}
function segundaBarra(){
  if(!document.body.classList.toggle("barraVacia")){
    document.body.classList.toggle("barraVacia");
  }
}
function barraInforme() {
  reset();
  colores(2);

  //Cambio en el siguiente menu
  btn1.innerText = "Crear Informe";
  btn1.setAttribute("href", "/informe/add");
  boton2s.innerText = "Ver Informes";
  boton2s.setAttribute("href", "/informe/view");
  boton3s.innerText = "";
  boton4s.innerText = "";
  boton5s.innerText = "";
  boton6s.innerText = "";
  segundaBarra();
  //tabla.innerHTML += `<tr><td></td><td></td><td></td><td></td><td></td><td><a href="#" class="view" title="View" data-toggle="tooltip"><i class="material-icons">&#xE417;</i></a><a href="#" class="edit" title="Edit" data-toggle="tooltip"><i class="material-icons">&#xE254;</i></a><a href="#" class="delete" title="Delete" data-toggle="tooltip"><i class="material-icons">&#xE872;</i></a><a href="#" class="pdf" title="pdf"data-toggle="tooltip"><i class="material-icons">	&#xe415;</i></a></td></tr> `;
}

function barraActa() {
  reset();
  colores(3);

  //Cambio en el siguiente menu
  btn1.innerText = "Crear Acta";
  btn1.setAttribute("href", "/acta/add");
  boton2s.innerText = "Ver Actas";
  boton2s.setAttribute("href", "/acta/view");
  boton3s.innerText = "";
  boton4s.innerText = "";
  boton5s.innerText = "";
  boton6s.innerText = "";
  segundaBarra();
  //tabla.innerHTML += `<tr><td></td><td></td><td></td><td></td><td></td><td><a href="#" class="view" title="View" data-toggle="tooltip"><i class="material-icons">&#xE417;</i></a><a href="#" class="edit" title="Edit" data-toggle="tooltip"><i class="material-icons">&#xE254;</i></a><a href="#" class="delete" title="Delete" data-toggle="tooltip"><i class="material-icons">&#xE872;</i></a><a href="#" class="pdf" title="pdf"data-toggle="tooltip"><i class="material-icons">	&#xe415;</i></a></td></tr> `;
}

function barraAuditor() {
  reset();
  colores(4);

  //Cambio en el siguiente menu
  btn1.innerText = "Listado";
  btn1.setAttribute("href", "/listado");
  boton2s.innerText = "Registro H.A.";
  boton2s.setAttribute("href", "/registro");
  boton3s.innerText = "Constancia";
  boton3s.setAttribute("href", "/constancia");
  boton4s.innerText = "";
  boton5s.innerText = "";
  boton6s.innerText = "";
  segundaBarra();
  //tabla.innerHTML += `<tr><td></td><td></td><td></td><td></td><td></td><td><a href="#" class="view" title="View" data-toggle="tooltip"><i class="material-icons">&#xE417;</i></a><a href="#" class="edit" title="Edit" data-toggle="tooltip"><i class="material-icons">&#xE254;</i></a><a href="#" class="delete" title="Delete" data-toggle="tooltip"><i class="material-icons">&#xE872;</i></a><a href="#" class="pdf" title="pdf"data-toggle="tooltip"><i class="material-icons">	&#xe415;</i></a></td></tr> `;
}

function barraLista() {
  reset();
  colores(5);

  //Cambio en el siguiente menu
  btn1.innerText = "Añadir Lista";
  btn1.setAttribute("href", "/lista/add");
  boton2s.innerText = "Ver Listas";
  boton2s.setAttribute("href", "/lista/view");
  boton3s.innerText = "";
  boton4s.innerText = "";
  boton5s.innerText = "";
  boton6s.innerText = "";
  segundaBarra();
  //tabla.innerHTML += `<tr><td></td><td></td><td></td><td></td><td></td><td><a href="#" class="view" title="View" data-toggle="tooltip"><i class="material-icons">&#xE417;</i></a><a href="#" class="edit" title="Edit" data-toggle="tooltip"><i class="material-icons">&#xE254;</i></a><a href="#" class="delete" title="Delete" data-toggle="tooltip"><i class="material-icons">&#xE872;</i></a><a href="#" class="pdf" title="pdf"data-toggle="tooltip"><i class="material-icons">	&#xe415;</i></a></td></tr> `;
}

function barraDatos() {
  reset();
  colores(6);

  //Cambio en el siguiente menu
  btn1.innerText = "Procesos";
  btn1.setAttribute("href", "/administracion/proceso");
  boton2s.innerText = "Dependencias";
  boton2s.setAttribute("href", "/administracion/dependencia");
  boton3s.innerText = "Clausulas";
  boton3s.setAttribute("href", "/administracion/clausula");
  boton4s.innerText = "Facultades";
  boton4s.setAttribute("href", "/administracion/facultad");
  boton5s.innerText = "Escuelas";
  boton5s.setAttribute("href", "/administracion/escuela");
  boton5s.innerText = "Usuarios";
  boton5s.setAttribute("href", "/user/usuariosLista");
  segundaBarra();
  //tabla.innerHTML += `<tr><td></td><td></td><td></td><td></td><td></td><td><a href="#" class="view" title="View" data-toggle="tooltip"><i class="material-icons">&#xE417;</i></a><a href="#" class="edit" title="Edit" data-toggle="tooltip"><i class="material-icons">&#xE254;</i></a><a href="#" class="delete" title="Delete" data-toggle="tooltip"><i class="material-icons">&#xE872;</i></a><a href="#" class="pdf" title="pdf"data-toggle="tooltip"><i class="material-icons">	&#xe415;</i></a></td></tr> `;
}
