function CrearFunciones() {
  jQuery("#funcionesTbody").append(
    '<tr><td><input style="width: 100%;" type="text" name="funciones" class="form-control" id="" placeholder="Funcion"></td></tr>'
  );
}
function AñadirAuditores() {
  jQuery("#auditoresTbody").append(strSelectAuditores());
}
function strSelectAuditores() {
  stringAuditores =
    '<tr><td><select name="auditores" class="form-control" id="">';

  for (var i = 0; i < dataJsonAuditores.length; i++) {
    stringAuditores +=
      '<option value="' +
      dataJsonAuditores[i].id +
      '">' +
      dataJsonUsuarios[i].nombre +
      "</option>";
  }

  stringAuditores += "</select></td></tr>";

  return stringAuditores;
}

function removerAuditores() {
  auditoresTbody.lastElementChild.remove();
}
function removerFunciones() {
  funcionesTbody.lastElementChild.remove();
}

let url = [{ listar: "/user/listarAuditores" }];

let dataJsonAuditores = [];
let dataJsonUsuarios = [];

const listarData = async () => {
  try {
    const responseAuditores = await fetch(url[0].listar);
    const dataAuditores = await responseAuditores.json();

    //Asignar procesos
    if (dataAuditores.message === "Success") {
      console.log(dataAuditores.objetos);
      console.log(dataAuditores.usuarios);
      dataJsonAuditores = dataAuditores.objetos;
      dataJsonUsuarios = dataAuditores.usuarios;
    } else {
      alert("Auditores o Usuarios no encontrados...");
    }
  } catch (error) {
    console.log(error);
  }
};

const cargaInicial = async () => {
  await listarData();
};

window.addEventListener("load", async () => {
  await cargaInicial();
});
