let dataJsonAuditores = [];

let url = [{ listar: "/user/listarAuditores" }];

function antiguoChangeCheckbox(id) {
  var elementoRemover = document.getElementById(id);
  var elementoInputStatus = document.getElementById(id).previousElementSibling;
  var elementoInputId =
    document.getElementById(id).previousElementSibling.previousElementSibling;

  if (elementoRemover.checked) {
    elementoInputStatus.value = "1";
  } else {
    elementoInputStatus.value = "0";
  }
}

const listarData = async () => {
  try {
    const responseAuditores = await fetch(url[0].listar);
    const dataAuditores = await responseAuditores.json();

    //Asignar procesos
    if (dataAuditores.message === "Success") {
      console.log(dataAuditores);
      dataJsonAuditores = dataAuditores;
    } else {
      alert("Auditores no encontrados...");
    }
  } catch (error) {
    console.log(error);
  }
};

const cargaInicial = async () => {
  await listarData();
};

window.addEventListener("load", async () => {
  await cargaInicial();
  inicialesLoad();
});

function CrearFunciones() {
  jQuery("#Funciones").append(
    '<tr><td><input style="width: 100%;" type="text" class="form-control" id="" placeholder="Funcion"></td></tr>'
  );
}
function AñadirAuditores() {
  jQuery("#Auditores").append("<tr><td>" + strSelectAuditores() + "</td></tr>");

  equipos_checks = document.getElementsByClassName("td7");
  for (var i = 0; i < equipos_checks.length; i++) {
    equipos_checks.item(i).innerHTML +=
      '<div class="col-md-4"><input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="0" name="auditor_checkbox_' +
      (i + 1) +
      '"/><label class="form-check-label" for="inlineCheckbox1">' +
      iniciales(
        Auditores.lastElementChild.firstElementChild.firstElementChild
          .firstElementChild.text
      ) +
      "</label></div>";
  }
}

function inicialesLoad() {
  labels = document.getElementsByClassName("antiguo-initials-name");

  for (var i = 0; i < labels.length; i++) {
    labels[i].innerHTML = iniciales(labels[i].innerHTML);
  }
}

function iniciales(nombre) {
  separador = " "; // un espacio en blanco
  arregloDeSubCadenas = nombre.split(separador); // SEPARA EL NOMBRE EN CADENAS INDIVIDUALES
  cadenaNueva = "";

  // IMPRIME LA PRIMERA LETRA DE CADA CADENA
  for (x = 0; x < arregloDeSubCadenas.length; x++) {
    cadenaNueva += arregloDeSubCadenas[x].substring(0, 1);
  }
  return cadenaNueva;
}

function cambioAuditor(id, num) {
  labels = [];
  checks = [];

  select = document.getElementById(id);
  select_id = select.value;
  select_nombre = select.options[select.selectedIndex].text;

  contenedores = document.getElementsByClassName("td7");

  for (var i = 0; i < contenedores.length; i++) {
    labels = labels.concat([
      contenedores[i].getElementsByTagName("label")[num],
    ]);
    contenedores[i].getElementsByTagName("label")[num].innerHTML =
      iniciales(select_nombre);
    contenedores[i].getElementsByTagName("input")[num].value = select_id;
    checks = checks.concat([
      contenedores[i].getElementsByTagName("input")[num],
    ]);
  }

  console.log(labels);
  console.log(checks);
}

function strSelectAuditores() {
  num = document.getElementsByClassName("select_auditor").length;

  stringAuditores =
    '<select class="select_auditor form-control" onchange="cambioAuditor(' +
    "'select_auditor_" +
    (num + 1) +
    "'" +
    "," +
    num +
    ')" id="select_auditor_' +
    (num + 1) +
    '">';

  for (var i = 0; i < dataJsonAuditores.objetos.length; i++) {
    stringAuditores +=
      '<option value="' +
      dataJsonAuditores.objetos[i].id +
      '">' +
      dataJsonAuditores.usuarios[i].nombre +
      "</option>";
  }

  stringAuditores += "</select>";

  return stringAuditores;
}

function AñadirTabla() {
  jQuery("#tableBOT").append(
    '<tr id = "proceso_1"><td id="proceso_1_td1"><fieldset class="form-group date-icon"><input type="date"class="col-md-10"style="margin-right: 10%;margin-top: 6%;margin-left: 2%;"id="fecha1_1"name="proceso_1_fechas"/><br /><br /></fieldset></td><td id="proceso_1_td2"><fieldset class="form-group date-icon"><input type="time"class="col-md-5"style="margin-right: 10%;margin-top: 6%;"id="fecha1_1"name="proceso_1_fechas"/><input type="time" class="col-md-5" style="margin-top: 6%;" id="fecha1_2" name="proceso_1_fechas"/><br /><br /></fieldset></td><td id="proceso_1_td3"><input style="width: 90%;" type="textarea" class="form-control" id="" placeholder="Lugar"></td><td id="proceso_1_td3"><input style="width: 90%;" type="textarea" class="form-control" id="" placeholder="Actividad"></td><td id="proceso_1_td3"><input style="width: 90%;" type="textarea" class="form-control" id="" placeholder="Proceso"></td><td id="proceso_1_td3"><div class="col-md-4"><input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1"> <label class="form-check-label" for="inlineCheckbox1">RD</label></div><div class="col-md-4"><input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="option2"> <label class="form-check-label" for="inlineCheckbox2">DF</label></div><div class="col-md-4"><input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="option2"> <label class="form-check-label" for="inlineCheckbox2">AR</label></div><div class="col-md-4"><input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="option2"> <label class="form-check-label" for="inlineCheckbox2">LA</label></div></td></tr>'
  );
}
function removerAuditores() {
  //tr
  var elementoRemover = Auditores.lastElementChild;
  iterator = 0;

  //Seleccion de elemento
  while (
    elementoRemover != null &&
    elementoRemover.id.includes("antiguo_auditor_tr") &&
    elementoRemover.style.display == "none"
  ) {
    elementoRemover = elementoRemover.previousElementSibling;
    iterator++;
  }

  if (elementoRemover == null) {
    return;
  }

  //Ocultacion
  if (elementoRemover.id.includes("antiguo_auditor_tr")) {
    //Ocultar y marcar auditor
    elementoRemover.style.display = "none";
    elementoRemover.firstElementChild.firstElementChild.nextElementSibling.value =
      "0";
    //Marcar checkboxes
    equipos_checks = document.getElementsByClassName("td7");
    for (var i = 0; i < equipos_checks.length; i++) {
      proceso_equipo_checks = equipos_checks
        .item(i)
        .getElementsByTagName("div");
      proceso_equipo_checks[
        proceso_equipo_checks.length - iterator - 1
      ].style.display = "none";
      input_status =
        proceso_equipo_checks[
          proceso_equipo_checks.length - iterator - 1
        ].getElementsByTagName("input");
      if (input_status.length >= 2) {
        input_status[1].value = "0";
      }
      console.log(
        proceso_equipo_checks[proceso_equipo_checks.length - iterator - 1]
      );
    }
  }
  //Eliminacion
  else {
    elementoRemover.remove();
    equipos_checks = document.getElementsByClassName("td7");

    for (var i = 0; i < equipos_checks.length; i++) {
      equipos_checks.item(i).lastElementChild.remove();
    }
  }
}
function removerFunciones() {
  Funciones.lastElementChild.remove();
}
function removerTabla() {
  tableBOT.lastElementChild.remove();
}
