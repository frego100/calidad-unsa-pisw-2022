let contadores = [[1, 1]];

let dataJsonProcesos = [];
let dataJsonDependencias = [];

let url = [
  { listar: "/administracion/listarProcesos" },
  { listar: "/administracion/listarDependencias" },
];

//const csrftoken = document.querySelector("[name=csrfmiddlewaretoken]").value;
var eliminados = { actividades: [1, 2, 3], dependencias: [4, 5, 6] };

function removeActividad(id, id_proceso) {
  var elementoRemover = document.getElementById(id).lastElementChild;
  if (elementoRemover.id != "") {
    elementoRemover.remove();
    contadores[id_proceso][0]--;
  }
}

function removeDependencia(id, id_proceso) {
  var elementoRemover = document.getElementById(id).lastElementChild;
  if (elementoRemover.id != "") {
    elementoRemover.remove();
    contadores[id_proceso][1]--;
  }
}

const listarData = async () => {
  try {
    const responseProcesos = await fetch(url[0].listar);
    const responseDependencias = await fetch(url[1].listar);
    const dataProcesos = await responseProcesos.json();
    const dataDependencias = await responseDependencias.json();

    //Asignar procesos
    if (dataProcesos.message === "Success") {
      console.log(dataProcesos.objetos);
      dataJsonProcesos = dataProcesos.objetos;
    } else {
      alert("Procesos no encontrados...");
    }

    //Asignar dependencias
    if (dataDependencias.message === "Success") {
      console.log(dataDependencias.objetos);
      dataJsonDependencias = dataDependencias.objetos;
    } else {
      alert("Dependencias no encontrados...");
    }
  } catch (error) {
    console.log(error);
  }
};

const cargaInicial = async () => {
  await listarData();
};

window.addEventListener("load", async () => {
  await cargaInicial();
});

function extraSelect(id, num) {
  contadores[num - 1][0] = contadores[num - 1][0] + 1;
  jQuery("#" + id).append(strSelectProcesos(num));
}

function extraSelect2(id, num) {
  contadores[num - 1][1] = contadores[num - 1][1] + 1;
  jQuery("#" + id).append(strSelectDependencias(num));
}

function CreateTable() {
  addProceso();
  jQuery("#table").append(
    '<tr id="proceso_' +
      n_procesos.value +
      '"><td id="proceso_' +
      n_procesos.value +
      '_td1"><button type="button"  class="btn btn-primary btn-sm "  onclick="extraSelect(' +
      "'proceso_" +
      n_procesos.value +
      "_td1'," +
      n_procesos.value +
      ')">Añadir</button> ' +
      ' <button type="button"class="btn btn-danger btn-sm pull-right" onclick="removeActividad(' +
      "'proceso_" +
      n_procesos.value +
      "_td1'," +
      (n_procesos.value - 1) +
      ')"><i class="fa fa-plus" aria-hidden="true"></i> Eliminar</button>' +
      strSelectProcesos(n_procesos.value) +
      '</td><td id="proceso_' +
      n_procesos.value +
      '_td2"><fieldset class="form-group date-icon">' +
      strSelectFechas(n_procesos.value) +
      '<br><br></fieldset></td><td id="proceso_' +
      n_procesos.value +
      '_td3">' +
      strSelectDependencias(n_procesos.value) +
      "</td></tr>"
  );
}

function addProceso() {
  n_procesos.value = parseInt(n_procesos.value) + 1;
  contadores = contadores.concat([[1, 1]]); //Agrega un arr de contadores
  console.log(n_procesos.value);
  console.log(contadores);
}

function removeProceso() {
  if (table.lastElementChild == null) {
    return;
  } else {
    table.lastElementChild.remove();
    n_procesos.value = parseInt(n_procesos.value) - 1;
    contadores.splice(contadores.length - 1); //Quita el ultimo de los contadores
  }

  console.log(n_procesos.value);
}

function strSelectProcesos(num) {
  stringProcesos =
    '<select class="form-control select2" id="proceso' +
    num +
    "_" +
    contadores[num - 1][0] +
    '" name="proceso_' +
    num +
    '_procesos"><option selected>Elige una opcion</option>';

  for (var i = 0; i < dataJsonProcesos.length; i++) {
    stringProcesos +=
      '<option value="' +
      dataJsonProcesos[i].id +
      '">' +
      dataJsonProcesos[i].nombre +
      "</option>";
  }

  stringProcesos += "</select>";

  return stringProcesos;
}

function strSelectDependencias(num) {
  stringDependencias =
    '<select class="form-control select2" id="dependencia' +
    num +
    "_" +
    contadores[num - 1][1] +
    '" name="proceso_' +
    num +
    '_dependencias"><option selected>Elige una opcion</option>';

  for (var i = 0; i < dataJsonDependencias.length; i++) {
    stringDependencias +=
      '<option value="' +
      dataJsonDependencias[i].id +
      '">' +
      dataJsonDependencias[i].nombre +
      "</option>";
  }

  stringDependencias += "</select>";
  return stringDependencias;
}

function strSelectFechas(num) {
  return (
    '<input type="date" class="col-md-5 " style="margin-right: 10%;margin-top: 6%;margin-left: 2%;" id="fecha' +
    num +
    '_1" name="proceso_' +
    num +
    '_fechas"><input type="date" style="margin-top: 6%;" class="col-md-5 " id="fecha' +
    num +
    '_2" name="proceso_' +
    num +
    '_fechas"></input>'
  );
}
