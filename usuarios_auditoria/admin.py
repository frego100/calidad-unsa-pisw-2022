from django.contrib import admin

from usuarios_auditoria.models import Usuario, Auditor

# Register your models here.
admin.site.register(Usuario)
admin.site.register(Auditor)