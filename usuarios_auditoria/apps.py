from django.apps import AppConfig


class UsuariosAuditoriaConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'usuarios_auditoria'
