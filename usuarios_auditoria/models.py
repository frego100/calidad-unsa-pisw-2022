from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from administracion.models import CorreoAdministrador

# Create your models here.
class Usuario(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    rol = models.CharField(max_length=100)

def create_rol_user(sender, **kwargs):
    correos_administradores = list(CorreoAdministrador.objects.filter())

    for index, correo_administrador in zip(range(len(correos_administradores)), correos_administradores):
        correos_administradores[index] = correo_administrador.correo

    if kwargs['created']:
        if kwargs['instance'].email in correos_administradores:
            usuario = Usuario.objects.create(
                user=kwargs['instance'], 
                rol = 'administrador'
            )
            auditor = Auditor.objects.create(
                usuario = usuario,
                estado = True
            )
        else:
            usuario = Usuario.objects.create(
                user=kwargs['instance'], 
                rol = 'invitado'
            )
            auditor = Auditor.objects.create(
                usuario = usuario,
                estado = False
            )

post_save.connect(create_rol_user, sender=User)

class Auditor(models.Model):
    usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE)
    dni = models.CharField(max_length=100, default="")
    dependencia = models.CharField(max_length=100, default="")
    cargo = models.CharField(max_length=100, default="")
    estado = models.BooleanField(default=True)

    def __str__(self):
        return self.usuario.user.first_name + " " +self.usuario.user.last_name