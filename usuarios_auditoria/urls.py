from django.urls import path, re_path
from . import views

urlpatterns = [
    path('get_users/', views.getUsers, name='getUsers'),
    path('get_account/', views.getAccount, name='getAccount'),
    path('vap/', views.vistaAuditorPrograma, name='vap'),
    path('perfil/', views.vistaUsuario, name='perfil'),
    path("principal/", views.principal, name="principal"),
    path("listarAuditores", views.listarAuditores, name='auditores'),
    re_path(r'usuarios/(?P<data_id>[0-9]+)$',
            views.EditUsuarios.as_view(), name='usuarios'),
    path("usuariosLista", views.UsuariosVista.as_view(), name='usuarios-lista'),
]
