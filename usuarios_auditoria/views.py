from contextlib import _RedirectStream
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from usuarios_auditoria.models import Usuario, Auditor
from django.views import View
import json
from django.http import JsonResponse
# Create your views here.
# view:
from django.core import serializers

def Login(request):
    if not request.user.is_authenticated:
        return  render(request,"login.html")
    else:
        return redirect("principal")

def listarAuditores(_request):
    auditores = list(Auditor.objects.filter(estado = True).values())
    nombres = {}
    for val, index in zip(auditores, range(len(auditores))):
        usuario_temp = Usuario.objects.get(id=val["usuario_id"])
        user = User.objects.get(id=usuario_temp.user.id)
        nombres[str(index)] = {"nombre": (
            user.first_name + " " + user.last_name)}
        #nombres = serializers.serialize('json', user)

    #print("Nombres", nombres)

    if (len(auditores) > 0):
        data = {'message': "Success", 'objetos': auditores, 'usuarios': nombres}
    else:
        data = {'message': "Not Found"}
    return JsonResponse(data)


def getUsers(request):
    users = User.objects.all()
    jsonUsers = []
    for x in users:
        jsonUsers.append({
            'Nombre': x.first_name,
            'Apellido': x.last_name,
            'Email': x.email,
        })
    return HttpResponse(
        json.dumps(jsonUsers, indent=3),
        content_type='application/json'
    )


def getAccount(request):
    p = request.user
    h = p.is_authenticated
    print("Usuario: ", request.user, h)
    return HttpResponse(
        json.dumps(
            {'message': p.username}, indent=3),
        content_type='application/json'
    )


def vistaAuditorPrograma(request):
    return redirect(principal)


def vistaUsuario(request):
    return  redirect("principal")
    
    pp = request.user
    ll = Usuario.objects.get(user=pp)
    context = {
        'auth': pp,
        'user': ll
    }

    return render(request, "perfil.html", context)


def rol_processor(request):
    rol = Usuario.objects.get(user=request.user)
    return {'rol': rol}


def principal(request):
    return redirect("view-programa")


############## USUARIOS #######################
class UsuariosVista(View):
    def get(self, request):
        if not request.user.is_authenticated:
            return  redirect("login")
        if request.user.is_authenticated and Usuario.objects.get(user=request.user).rol != "administrador":
            return  redirect("principal")
        
        auth_usuario = request.user
        context = {}
        if request.user.is_authenticated:
            usuario = Usuario.objects.get(user=auth_usuario)
            context["user"] = usuario
            usuarios =  Usuario.objects.all()
            
            context["usuarios"] = usuarios
            auditores = Auditor.objects.filter()
            context["auditores"] = auditores
            ids = []
            for x in auditores:
                ids += [x.usuario]
            
            context["ids_auditores"] = ids
            return render(request, "usuarios.html", context)
        return redirect("principal")

class EditUsuarios (View):
    def get(self, request, *args, **kwargs):
        usuarios = Usuario.objects.get(id=self.kwargs['data_id'])

        context = {
            'usuarios': usuarios
        }
        return render(request, "usuarios.html", context)

    def post(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return  redirect("login")
        if request.user.is_authenticated and Usuario.objects.get(user=request.user).rol != "administrador":
            return  redirect("principal")
        
        usuarios = Usuario.objects.get(id=self.kwargs['data_id'])
        usuarios.rol = request.POST.get("edit_rol")
        seleccion = request.POST.get("seleccion_edit")
        if seleccion == "1":
            usuarios.rol = "administrador"
        elif seleccion == "2":
            usuarios.rol = "auditor"
        else :
            usuarios.rol = "invitado"
        usuarios.save()


        if  usuarios.rol == "administrador" or usuarios.rol == "auditor" :      
            if request.POST.get("edit_audi_id") != '':
                audit = Auditor.objects.get(id=request.POST.get("edit_audi_id"))
                audit.dni =  request.POST.get("edit_dni") 
                audit.dependencia =  request.POST.get("edit_dependencia") 
                audit.cargo =  request.POST.get("edit_cargo")
                audit.estado = True
                audit.save()
            else :
                aux = Auditor.objects.get(usuario=usuarios)
                if aux != None:
                    aux.dni =  request.POST.get("edit_dni") 
                    aux.dependencia =  request.POST.get("edit_dependencia") 
                    aux.cargo =  request.POST.get("edit_cargo")
                    aux.estado = True
                    aux.save()
                else: 
                    audit = Auditor.objects.create (
                        usuario = usuarios,
                        dni  = request.POST.get("edit_dni"),
                        dependencia = request.POST.get("edit_dependencia") ,
                        cargo =request.POST.get("edit_cargo"),
                        estado = True 
                    ) 
        else :
            if request.POST.get("edit_audi_id") != '':
                audit = Auditor.objects.get(id=request.POST.get("edit_audi_id"))
                audit.estado = False
                audit.save()
 
        return redirect ("usuarios-lista")
